## rabbiter-base(v 1.0.0.RELEASE)框架说明
![](https://rabbiter.top/favicon.ico)

rabbiter-base仓库地址：[https://gitee.com/w_rabbiter/rabbiter-base](https://gitee.com/w_rabbiter/rabbiter-base) <br>
本文出自：[Rabbiter | 正华心琴站](https://rabbiter.top/#/seeBlog?blogId=123&token=38DB3AED920CF82AB059BFCCBD02BE6A)

## 一. 前言
### 1. 为什么我编写了rabbiter-base框架
在实习工作过程中受到的启发，我决定编写一个可以简化自己项目开发的框架，后续版本我会考虑框架定制的方案。当然项目开源，可供下载参考（仅供参考，欢迎意见）<br>
目前主流的Java Web项目基本都基于Spring Boot快速搭建，这也为微服务提供更便利的开发手段。<br>
但是基于我所写Spring Boot的项目，仍有有大量的重复代码（例如实体类、数据访问层、业务层等）。并且在我们手动编写这些代码过程中还会出现各种bug。<br>
明显这些代码是可以交给程序去构建的（类似MyBatis-Plus的代码生成器，当然也可以使用rabbiter-base框架提供的rabbiter-builder模块去构建代码，生成的代码可以无缝搭配rabbiter-framework框架使用）。<br>
而rabbiter-framework则是rabbiter-base的主要部分。<br>
我编写rabbiter-framework模块的初衷就是简化代码，尽可能提取重复代码，封装公共业务层、公共报文等，让我们可以更加把开发集中在处理业务上。<br>
鉴于开发过程中的各种问题，我会不断优化rabbiter-framework。当然我也正处于实习阶段，很多问题考虑不周，有什么建议或技术问题可以指出。<br>
虽然目前框架的局限性还是存在的（我会在以后的版本迭代中优化），但是对于开发Spring Boot项目开发效率的提升还是比较明显的。<br>
后期我还会添加其他模块。诸如rabbiter-cloud服务模块、rabbiter-jms消息服务模块、rabbiter-cache缓存等模块，初衷就是为了简化微服务开发，让懒惰发挥的淋漓尽致。

### 2. 框架组成
#### (1) rabbiter-builder：代码构造器
以下简称builder<br>
用于根据数据库表结构快速构建springboot工程，目前可以为你的工程构造：<br>
- 实体类
- DAO接口
- 业务接口
- 业务类
- 控制类
- Mapper映射文件等<br>

项目依赖了rabbiter-framework开发框架。
#### (2) rabbiter-framework：开发框架
以下简称framework<br>
rabbiter-framework框架依赖了：spring boot、mybatis-plus、swagger2<br>
为了快速开发web项目，rabbiter-framework目前提供了以下几个模块：
- 公共业务接口（BaseService）<br>
目前实现了插入数据、批量插入数据、删除数据、批量删除数据、更新数据、批量更新数据、根据主键获取数据、根据指定列和值获取指定数据列表等
- 公共API接口报文返回值（BaseApi）<br>
返回报文格式均为：
```json
{
    "code":"",
    "msg":"",
    "data":{}
}
```
详情可以查看BaseApi类的定义

## 二. 适用场景
开始使用rabbiter-base框架前，有以下必备要求：
- 要求jdk版本1.8及以上<br>
- 构造的项目基于lombok插件简化代码，所以要求ide已安装lombok插件

当然我还建议最好满足以下要求：
- 实体属性统一采用驼峰命名法，数据库字段统一采用下划线命名法
- 每张表最好都设置好注释，每张表的字段最好都设置好注释，生成的类或属性会根据你定义的注释生成注释
- 对于数据库表的设计，每张表最好均存在四个字段：id（自增主键）、created（创建时间）、updated（更新时间）、version（版本），因为生成的实体类如果包含四个字段，builder会为你生成相应注解：
```java
public class BlogCommentDO implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @Version
    private Integer version;
}
```
当然不存在也无妨，你可以根据需求自己添加。<br>
对此，rabbiter-framework提供了一个BaseMybatisPlusConfig抽象类，builder为你生成的代码中MybatisPlusConfig类会自动继承BaseMybatisPlusConfig抽象类<br>
目前可以为你在新增操作自动填充字段created和updated，在更新操作中自动填充updated。（使用mybatis-plus提供的api时）<br>
你可以根据情况在MybatisPlusConfig类中重写BaseMybatisPlusConfig的insertFill()和updateFill()方法。

## 三. 初始化rabbiter-base框架（可跳过）
该框架已上传中央仓库，该步骤**可以跳过**，这里我只是提一下手动install步骤。
可以按下列步骤导出框架到maven仓库：
- 克隆rabbiter-base源码，仓库链接：https://gitee.com/w_rabbiter/rabbiter-base
- 导入rabbiter-base工程（推荐使用IntelliJ IDEA）
- 在rabbiter-base目录下执行maven命令：
```
mvn clean install
```
- 观察控制台是否构建完成
- 到这一步就将rabbiter-base导出到了本地仓库，开始使用框架吧。

## 四. 开始使用rabbiter-base框架
### 1. 创建一个空的maven工程
不需要引入任何其他依赖，仅需引入以下列出的依赖。<br>
rabbiter-base会帮你统一管理并引入所需其他基本依赖。
### 2. 引入rabbiter-base框架
#### (1) 添加父依赖
```xml
<parent>
    <groupId>top.rabbiter</groupId>
    <artifactId>rabbiter-base</artifactId>
    <version>1.0.0.RELEASE</version>
    <relativePath/>
</parent>
```
#### (2) 引入rabbiter-builder
```xml
<dependency>
    <groupId>top.rabbiter</groupId>
    <artifactId>rabbiter-builder</artifactId>
</dependency>
```
#### (3) 引入rabbiter-framework
```xml
<dependency>
    <groupId>top.rabbiter</groupId>
    <artifactId>rabbiter-framework</artifactId>
</dependency>
```
### 3. 编辑pom.xml文件
#### (1) mysql-connection
该依赖根据自身数据库版本选择,但是需要保证版本5.1.38以上(mybatis-plus支持)
```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.11</version>
</dependency>
```
#### (2) 配置读取.yml资源
如果在进行 “3. 使用rabbiter-builder构造spring boot工程” 时找不到.yml文件，需要引入以下配置
```xml
<build>
    <resources>
        <!-- 配置编译时不对配置文件进行过滤 -->
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*.yml</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>false</filtering>
        </resource>
    </resources>
</build>
```
#### (3) 完整的pom文件示例
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <!-- 父依赖 begin -->
    <parent>
        <groupId>top.rabbiter</groupId>
        <artifactId>rabbiter-base</artifactId>
        <version>1.0.0.RELEASE</version>
        <relativePath/>
    </parent>
    <!-- 父依赖 end -->
    <groupId>com.id0304.demo</groupId>
    <artifactId>demo</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <!-- rabbiter-base的子工程 begin -->
        <dependency>
            <groupId>top.rabbiter</groupId>
            <artifactId>rabbiter-builder</artifactId>
        </dependency>
        <dependency>
            <groupId>top.rabbiter</groupId>
            <artifactId>rabbiter-framework</artifactId>
        </dependency>
        <!-- rabbiter-base的子工程 end -->

        <!-- MySQL驱动包 begin -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <!-- 根据所用MySQL版本引入对应驱动程序 -->
            <version>8.0.21</version>
        </dependency>
        <!-- MySQL驱动包 end -->
    </dependencies>

    <build>
        <resources>
            <!-- 配置编译时不对配置文件进行过滤 -->
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.yml</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
        </resources>
    </build>
</project>
```
## 五. 使用rabbiter-builder构造spring boot工程
### 步骤一. 创建构造器
#### 1. 方式一（注解，推荐方式）
**通过注解进行项目构造**<br>
该方式构造工程只需要提供一个构造类，并添加对应注解即可：
```java
@RBuilder(
        application = @Application(name = "rabbiter-demo"),
        database = @Database(
                driver = "com.mysql.cj.jdbc.Driver",
                url = "jdbc:mysql://localhost:3306/rabbiter_v2?serverTimezone=Asia/Shanghai&characterEncoding=UTF8&useSSL=false",
                username = "root",
                password = "root"
        ),
        basePackage = @BasePackage("com.rabbiter.demo")
)
public class Builder {
    public static void main(String[] args) {
        RabbiterBuilder.run(Builder.class);
    }
}
```
上是简化版的注解，如果您想自定义生成类的内容，可以参考下面的配置：

```java
@RBuilder(
        application = @Application(name = "rabbiter-demo"),
        database = @Database(
                driver = "com.mysql.jdbc.Driver",
                url = "jdbc:mysql://localhost:3306/rabbiter_v2?serverTimezone=Asia/Shanghai&characterEncoding=UTF8&useSSL=false",
                username = "root",
                password = "root"
        ),
        basePackage = @BasePackage("com.rabbiter.demo"),
        targets = @Targets(
                entity = @Target(targetPackage = "entity", suffix = "DO"),
                dto = @Target(targetPackage = "dto", suffix = "DTO"),
                dao = @Target(targetPackage = "dao", suffix = "DAO"),
                service = @Target(targetPackage = "service", suffix = "Service"),
                serviceImpl = @Target(targetPackage = "service.impl", suffix = "ServiceImpl"),
                controller = @Target(targetPackage = "controller", suffix = "Controller"),
                config = @Target(targetPackage = "config", suffix = "Config")
        )
)
public class Builder {
        public static void main(String[] args) {
                RabbiterBuilder.run(Builder.class);
        }
}
```
接下来只需要执行**main方法**就可以构造出工程。<br>
此方法必须在构造类上标注@RBuilder，且调用方法RabbtierBuilder.run()时必须传入参数XXX.class（即当前构造类）。<br>
下面我编写了一下@RBuilder注解相关文档：
|注解|作用|备注|
|-|-|-|
|@RBuilder|构造器注解。包含四个属性：application（应用信息）、database（数据源信息）、basePackage（基础包信息）、targets（目标文件信息）|必须标注|
|@Application|标注生成的应用信息，目前只需标注生成的应用名称|赋值在构造器注解的application属性上。必须标注|
|@Database|标注数据源信息，工程全部依赖指定的数据库生成|赋值在构造器注解的database属性上。必须标注|
|@BasePackage|标注生成的基础包信息|赋值在构造器注解的basePackage属性上。必须标注|
|@Targets|管理生成的目标文件信息。目前可以指定属性：entity（实体类）、dto（传输数据模型）、dao（持久层）、service（业务接口）、serviceImpl（业务类）、controller（控制层）、config（配置类）。这些属性均通过@Target注解标注|赋值在构造器注解的targets属性上。有默认值。可选标注|
|@Target|标注生成的目标文件信息。包含两个属性：targetPackage（标注生成子包的信息）、suffix（生成文件后缀名信息）|赋值在@Targets注解的entity、dto、dao、service、serviceImpl、controller、config属性上。有默认值，可选标注，且默认值如上面的例子所示|

#### 2. 方式二（配置文件）
**通过配置文件进行项目构造**
#### (1). 在resources目录下创建rabbiter-builder.yml文件
- 必须修改的配置：只需要修改数据库连接信息即可快速构建项目
- 其他配置项：可以自定义配置其他内容，可定义内容如下
```yaml
# rabbiter代码构造器配置文件，代码构建完成之后可以自行删除
rabbiter:
  # 项目工程/应用相关配置
  application:
    # 项目名称
    name: demo
  # 数据库配置
  jdbc:
    driver: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/demo?serverTimezone=Asia/Shanghai&characterEncoding=UTF8&useSSL=false
    username: root
    password: root
  #==========================================以下配置根据项目需求更改========================================================
  # 生成目标类配置
  target:
    # 基础包名
    base: com.rabbiter.demo
    # 实体类配置
    entity:
      # 实体类存放的包名，下同
      package: entity
      # 生成实体类名的后缀，下同（注意：除了实体类的后缀可以设置成空字符串，其余类的后缀均不可以设置成空字符串）
      suffix: DO
    # DAO层配置
    dao:
      package: dao
      suffix: Mapper
    # 业务层配置
    service:
      package: service
      suffix: Service
    # 业务实现类配置
    serviceImpl:
      package: service.impl
      suffix: ServiceImpl
    # 控制层/rpc层配置
    controller:
      package: controller
      suffix: Controller
    # 传输数据模型类配置
    dto:
      package: dto
      suffix: DTO
    # 配置类配置
    config:
      package: config
```
#### (2). 创建构造类 
只需要执行代码：RabbiterBuilder.run();<br>
可以自行创建一个主线程运行以下代码，运行完毕即可删除
```java
/**
 * 运行主方法，运行完毕就可以自行删除
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/8
 */
public class Builder {
    public static void main(String[] args) {
        RabbiterBuilder.run();
    }
}
```
### 步骤二. 构造结果
控制台出现以下图案，即构造完成
```
 _____                                          
/  ___|                                         
\ `--.   _   _    ___    ___    ___   ___   ___ 
 `--. \ | | | |  / __|  / __|  / _ \ / __| / __|
/\__/ / | |_| | | (__  | (__  |  __/ \__ \ \__ \
\____/   \__,_|  \___|  \___|  \___| |___/ |___/
```
### 步骤三. 测试生成工程
构造完毕之后，我们能看到基本包、类文件、配置文件、Spring Boot启动类均已生成。<br>
运行Spring Boot的启动器看看项目能否正常启动。<br>
启动后，观察控制台是否无误。<br>
可以访问localhost:8080/swagger-ui.html查看接口文档是否能正常访问。

## 六. 使用rabbiter-framework快速开发web工程
### 1. 接口增强（Controller/Rpc）
#### (1) BaseResponseBodyAdvice
组件位置：com.rabbiter.framework.aop.BaseResponseBodyAdvice<br>
该组件提供了一个接口统一报文响应格式，并且在rabiter-builder为你生成代码后，不需要做任何配置就可以使用，其格式如下：
```json
{
    "code": "00000",
    "data": {},
    "errorMsg": ""
}
```
code为状态码、data为携带的数据、errorMsg为错误信息<br>
其中code为必须参数，默认返回00000为响应成功（对于状态码定义我参照了阿里巴巴开发手册的状态码定义，目前只定义了一部分状态码于枚举BaseApiCode中）<br>
当然，你也可以不使用rabbiter-framework为你提供的报文格式，操作步骤如下：<br>
细心一点可以发现rabbiter-builder为你生成了一个aop包下的ApiResponseBodyAdvice类，该类就是响应接口增强器，内容如下：
```java
@RestControllerAdvice({"{controllerPackage}"})
public class ApiResponseBodyAdvice extends BaseResponseBodyAdvice {

}
```
该类继承了BaseResponseBodyAdvice抽象类，就是framework为你提供的一个公共报文增强器，你可以不进行对该类的继承，从而解除统一报文格式封装，甚至删除为你生成的ApiResponseBodyAdvice类。

#### (2) ApiException
该异常是rabbiter-framework为你提供的一个公用接口异常，目前拥有两个构造函数：<br>
**构造函数1：**
> public ApiException(String code, String message);

**构造函数2：**
> public ApiException(ApiCode apiCode, String message);

构造函数1你可以自定义状态码<br>
而构造函数2则使用framework为你提供的一组状态码ApiCode（基于阿里巴巴开发手册状态码定义）<br>
如果你在接口中抛出了某个ApiException，则框架会为你封装成报文，内容就是上面BaseResponseBodyAdvice为你提供的格式，并携带错误信息，下面是一个示例：
我们定义一个接口：
```java
@RestController
@RequestMapping("/api/blog")
public class BlogController {
    @GetMapping("/get")
    public BlogDO get(Long id) throws ApiException {
        if(ObjectUtils.isEmpty(id)) {
            //抛出ApiException异常
            throw new ApiException(ApiCode.CODE_A0400, "传入参数的id异常");
        }
        return blogService.getById(id);
    }
}
```
如果我们没有传入参数id，则得到的响应结果会是：
```json
{
    "code": "A0400",
    "data": "",
    "errorMsg": "传入参数的id异常"
}
```

#### (3) @IgnoreBaseResponse
基于灵活性的调整，我在rabbiter-framework中引入了一个注解@IgnoreBaseResponse，该注解可以绕过BaseResponseBodyAdvice的报文封装。即我们如果希望一个控制类或者方法不进行响应报文的封装，就可以在对应的控制类上或方法上标注@IgnoreBaseResponse，如果在类上标注，则类下面的所有接口（方法）返回值都不会被BaseResponseBodyAdvice包装

#### (4) ApiCode
rabbiter-framework基于阿里巴巴开发手册状态码定义的状态码枚举，你可以根据需求去使用他们，一般配合异常ApiException使用。

#### (5) @Valid
@Valid是hibernate-validator提供的一个注解，rabbiter-framework也对该注解进行了一些封装，方便我们使用。
针对接口传入值，我们可以通过使用@Valid对参数进行统一校验，示例如下：
```java
@RestController
@RequestMapping("/api/blog")
public class BlogController {
    @PostMapping("/add")
    public Boolean add(@Valid @RequestBody AddBlogReqDTO reqDTO) {
        return blogService.add(reqDTO);
    }
}
```
我们在传入的参数reqDTO前加上了@Valid注解，即开启了对该参数的校验。接着我们来看看AddBlogReqDTO这个类：
```java
public class AddBlogReqDTO {
    @NotNull(message = "标题不能为null")
    private String title;
}
```
该类的属性title上加了一个@NotNull注解，当传入的title为null时，就会被检验到非法参数，返回统一报文（该报文也是在BaseResponseBodyAdvice里进行了封装），响应内容如下：
```json
{
    "code": "A0400",
    "data": "",
    "errorMsg": "[标题不能为null]"
}
```
我们可以看到参数code返回的是A0400，表示参数错误。errorMsg返回的则是我们在注解@NotNull中定义的message的值。<br>
具体的其他注解可以自行查阅，我这里也列举一些常用注解：<br>
|注解|作用|
|-|-|
|@Null|限制只能为null|
|@NotNull|限制必须不为null|
|@AssertFalse|限制必须为false|
|@AssertTrue|限制必须为true|
|@DecimalMax(value)|限制必须为一个不大于指定值的数字|
|@DecimalMin(value)|限制必须为一个不小于指定值的数字|
|@Digits(integer,fraction)|限制必须为一个小数，且整数部分的位数不能超过integer，小数部分的位数不能超过fraction|
|@Future|限制必须是一个将来的日期|
|@Max(value)|限制必须为一个不大于指定值的数字|
|@Min(value)|限制必须为一个不小于指定值的数字|
|@Past|限制必须是一个过去的日期|
|@Pattern(value)|限制必须符合指定的正则表达式|
|@Size(max,min)|限制字符长度必须在min到max之间|
|@Past|验证注解的元素值（日期类型）比当前时间早|
|@NotEmpty|验证注解的元素值不为null且不为空（字符串长度不为0、集合大小不为0）|
|@NotBlank|验证注解的元素值不为空（不为null、去除首位空格后长度为0），不同于@NotEmpty，@NotBlank只应用于字符串且在比较时会去除字符串的空格|
|@Email|验证注解的元素值是Email，也可以通过正则表达式和flag指定自定义的email格式|

### 2. 业务增强(Service/Manager)
#### (1) BaseService\<T\>
通过rabbiter-builder生成代码后，所有的业务接口都继承了一个父接口，就是BaseService<br>
该接口定义一系列对数据的基本业务操作（增删改查），我们可以直接调用这些方法达到快速增删改查，下面给出一个例子。<br>
业务接口（没有定义任何方法，但是继承了BaseService）：
```java
public interface BlogService extends BaseService<BlogDO> {

}
```
控制层（仅调用了BaseService的getById（Serializable id）方法）：
```java
@RestController
@RequestMapping("/api/blog")
@Api(tags = "博客模块接口")
public class BlogController {
    Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private BlogService blogService;

    @GetMapping("/get")
    public BlogDO get(Long id) throws ApiException {
        log.info("#####获取博客");
        if(ObjectUtils.isEmpty(id)) {
            throw new ApiException(BaseApiCode.CODE_A0400, "传入参数的id异常");
        }
	//调用BaseService的getById（Serializable id）方法
        return blogService.getById(id);
    }
}
```
就可以完成整个根据id获取实体（BlogDO）的流程，仅需在controller里插入少量代码就完成一个简单接口。<br>
目前完成的一些统一业务操作：
```
    /**
     * 插入数据
     *
     * @param t 数据对象
     * @return 是否插入成功
     */
    Boolean insert(T t);

    /**
     * 根据主键更新数据
     *
     * @param t 更新数据对象
     * @return 是否更新成功
     */
    Boolean update(T t);

    /**
     * 批量插入数据 - 不适用于Oracle
     *
     * @param collection 插入数据集合
     * @return 是否插入成功
     */
    Boolean batchInsert(Collection<T> collection);

    /**
     * 批量更新数据 - 不适用于Oracle
     *
     * @param collection 更新数据集合
     * @return 是否更新成功
     */
    Boolean batchUpdate(Collection<T> collection);

    /**
     * 根据主键删除数据
     *
     * @param id 主键
     * @return 是否删除成功
     */
    Boolean deleteById(Serializable id);

    /**
     * 根据主键批量删除数据
     *
     * @param ids 主键列表
     * @return 是否删除成功
     */
    Boolean batchDeleteByIds(Collection<T> ids);

    /**
     * 根据id获取对象
     *
     * @param id 主键
     * @return 对象
     */
    T getById(Serializable id);

    /**
     * 根据指定列查询列表
     *
     * @param t       封装查询的条件，跟传入的columns字段要对上
     * @param columns 条件字段，传入格式："id,name,sex"
     * @return 列表
     */
    List<T> getByColumns(T t, String columns);
```
后面我也会根据框架使用情况，增加优化更多的业务方法，提供更加效率的开发体验

#### (2) BaseServiceImpl\<T\>
BaseService\<T\>的实现类，由业务逻辑接口的实现类去继承，即BaseService\<T\>方法的具体实现，感兴趣的可以查阅源码。

#### (3) 业务增强API文档
#### BaseService<T>.getByColumns()
#### BaseService<T>.getByColumns(String resultColumns)
#### BaseService<T>.getByColumns(String resultColumns, T t, String conditionColumns)
**参数说明**
|参数|是否可以为null|说明|示例|备注|
|-|-|-|-|-|
|resultColumns|是，为null时返回所有字段。framework也提供对应重载函数。|结果集，目标返回的列表结果集。即需要查询的列表的结果需要包含哪些字段。|"age,sex"|逗号直接不能存在多余空格|
|t|是，为null时不做条件筛选。framework也提供对应重载函数。|条件值|User(age=18,sex="男")|传入的就是作为条件的值，需传入引用数据类型，只有与参数conditionColumns条件列表对上才有效果|
|conditionColumns|是，为null时不做条件筛选。framework也提供对应重载函数。|条件列表|"age,sex"|跟数据库定的字段列表需要对上|

**示例**
以下举一个查询用户信息的例子
```
User user = new User();
user.setAge(18);
user.setSex("男");
//查询用户，指定查询条件age = 18 , sex = '男'
List<User> userList = userService.getByColumns("id,name,age,sex", user, "age,sex");
```
最终获得的userList的值将会是（返回的列将会是resultColumns指定的值）：
```json
[
    {
        "id":"1",
        "name":"zhangsan",
        "age":18,
        "sex":"男"
    }, {
        "id":"9",
        "name":"lisi",
        "age":18,
        "sex":"男"
    }
]
```

### 3. Redis(BaseRedisService)

### 4. 公共Bean(BaseBean)
framework提供了一个公共Bean，封装了一些常用方法，可以让使用者更加方便地使用Bean，包含内容如下：
#### (1) toBean()
Bean与Bean之间的转化在开发中很常见，这里以传输数据模型（DTO）转实体模型（DO）为例子。
传输数据模型只需要实现接口BaseBean：
```java
@Data
public class UserDTO implements BaseBean {
    private String username;

    private String password;

    private String sex;
}
```
我们需要转换的目标Bean（实体模型）：
```java
@Data
public class UserDO implements BaseBean {
    private String username;

    private String password;

    private String sex;
}
```
那么就可以很快捷的将DTO转化为DO：
```java
public class Demo {
    public static void main(String[] args) {
        UserDTO sourceBean = new UserDTO();
        UserDO targetBean = new UserDO();
        sourceBean.setUsername("zhangsan");
        sourceBean.setPassword("123456");
        sourceBean.setSex("男");
        System.out.println(sourceBean.toBean(UserDO.class).toString());
    }
}

//控制台信息：
TargetBean(username=zhangsan, password=123456, sex=男)
```
**@BeanField**：此外，为面对DTO与DO属性有差异以及选择性属性转化属性的场景，framework提供了一个注解：。
@BeanField包含两个可选属性：
|属性名|默认值|作用|
|-|-|-|
|value|""|指定映射目标Bean的属性名。|
|ignore|false|是否忽略属性。指定该属性时value属性无效。|
比如上面的场景中，我们的DTO改为下面所示：
```java
@Data
public class UserDTO implements BaseBean {
    private String username;

    private String psw;

    private String sex;
}
```
password属性改为了psw，跟实体模型的password没有对应，则可以使用@EntityField注解指定value属性进行映射方可正常转换：
```java
@Data
public class UserDTO implements BaseBean {
    private String username;

    @BeanField("password")
    private String psw;

    private String sex;
}
```
如果你不想某个属性被转化，只需指定ignore属性:（注意：ignore属性存在时，value属性会失效）
```java
@Data
public class UserDTO implements BaseBean {
    private String username;

    private String psw;

    @BeanField(ignore = true)
    private String sex;
}
```
上面的例子sex属性将不会被拷贝。
#### (2) toJson()
该方法将当前Bean转化为json字符串：
```java
public class Demo {
    public static void main(String[] args) {
        UserDTO sourceBean = new UserDTO();
        sourceBean.setUsername("zhangsan");
        sourceBean.setPsw("123456");
        sourceBean.setSex("男");
        System.out.println(sourceBean.toJson());
    }
}

//控制台输出
{"psw":"123456","sex":"男","username":"zhangsan"}
```


### 5. 工具篇（Util）
#### (1) Cookie工具类（CookieUtil）

