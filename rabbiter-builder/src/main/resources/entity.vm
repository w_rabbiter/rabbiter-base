package ${package}.${entityPackage};

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import top.rabbiter.framework.bean.BaseBean;
#set($hasDate=0)
#set($hasBigDecimal=0)
#foreach($column in $columns)
#if($column.javaType=='LocalDateTime' && $hasDate==0)
import java.time.LocalDateTime;
#set($hasDate=1)
#end
#if($column.javaType=='BigDecimal' && $hasBigDecimal==0)
import java.math.BigDecimal;
#set($hasBigDecimal=1)
#end
#end
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 描述：${tableComment}数据模型
 *
 * @author Rabbiter
 * @version 1.0
 * @since ${date}
 */
@Setter
@Getter
@ToString
@EqualsAndHashCode
@Accessors(chain = true)
@TableName("${tableName}")
@ApiModel(description = "${tableComment}")
public class ${name}${entitySuffix} implements BaseBean {
    private static final long serialVersionUID = 1L;

#foreach($column in $columns)
#if($column.columnName == 'version')
    @Version
    @TableField(fill = FieldFill.UPDATE)
#end
#if($column.isPk)
    @TableId(value = "${column.columnName}", type = IdType.AUTO)
#end
#if($column.columnName == 'created')
    @TableField(fill = FieldFill.INSERT)
#end
#if($column.columnName == 'updated')
    @TableField(fill = FieldFill.INSERT_UPDATE)
#end
#if($column.columnName != $column.javaFieldName && !$column.isPk)
    @TableField("${column.columnName}")
#end
    @ApiModelProperty("${column.columnComment}")
    private ${column.javaType} ${column.javaFieldName};

#end
}