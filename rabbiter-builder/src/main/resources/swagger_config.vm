package ${package}.${configPackage};

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2配置类
 *
 * @author Rabbiter
 * @version 1.0
 * @since ${date}
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
    /**
     * 生成文档的配置,根据需求修改
     *
     * @return Docker
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.basePackage("${package}"))
            .paths(PathSelectors.any())
            .build();
    }

    /**
     * 接口文档信息,根据需求修改
     *
     * @return ApiInfo
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("${applicationName} Api文档")
            .description("Build By Rabbiter Framework")
            .version("1.0")
            .build();
    }
}