package top.rabbiter.builder.executor;

import org.springframework.util.CollectionUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.handler.StructureHandler;
import top.rabbiter.builder.handler.TableHandler;

/**
 * 代码构建启动器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class RabbiterBuilderExecutor implements TableHandler, StructureHandler {
    /**
     * 代码构建
     *
     * @throws RabbiterBuilderException 框架运行期异常
     */
    public void execute() throws RabbiterBuilderException {
        //1 获取全局配置
        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();

        //2 加载获取TableEntity列表对象到上下文
        loadTableEntity();

        //3 判空
        if(CollectionUtils.isEmpty(configContext.getTableEntityList())) {
            new RabbiterBuilderException("The Database don't have any tables").printStackTrace();
            return;
        }

        //4 依次生成对应文件
        for (TableEntity tableEntity : configContext.getTableEntityList()) {
            //开始构造项目
            create(tableEntity);
        }
    }
}
