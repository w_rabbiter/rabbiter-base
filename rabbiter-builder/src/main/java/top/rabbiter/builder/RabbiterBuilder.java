package top.rabbiter.builder;

import top.rabbiter.builder.adapter.BuilderAdapter;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.executor.RabbiterBuilderExecutor;
import top.rabbiter.builder.handler.BuilderHandler;
import org.springframework.util.ObjectUtils;

/**
 * Rabbiter-Builder 启动器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class RabbiterBuilder extends BuilderHandler {
    /**
     * 启动构造器
     *
     * @param clazz 构造类
     */
    public static void run(Class clazz) {
        try {
            //检查@RBuilder注解
            checkClazz(clazz);
            //获取构造器
            BuilderAdapter builderAdapter = getBuilder(clazz);
            //开始执行构造器
            builderAdapter.run(clazz);
        } catch (RabbiterBuilderException e) {
            e.printStackTrace();
        }
    }
}
