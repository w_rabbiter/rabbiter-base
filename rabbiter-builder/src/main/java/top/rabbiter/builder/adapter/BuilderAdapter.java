package top.rabbiter.builder.adapter;

import org.springframework.util.ObjectUtils;
import top.rabbiter.builder.annotation.*;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.util.StringUtil;

/**
 * 描述：构造器适配器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/25
 */
public interface BuilderAdapter {
    /**
     * 启动构造器
     *
     * @param clazz 启动构造器的基类（或主方所属类）
     */
    void run(Class clazz);

    /**
     * 验证@Rbuilder中的@Database(url)是否定义，未定义抛出异常
     *
     * @param clazz RBuilder注解所在基类
     * @throws RabbiterBuilderException 构造器异常
     */
    default void checkDatabaseAnnotation(Class<Object> clazz) throws RabbiterBuilderException {
        RBuilder rBuilder = clazz.getAnnotation(RBuilder.class);
        if (ObjectUtils.isEmpty(rBuilder)) {
            throw new RabbiterBuilderException("@RBuilder must be defined");
        }
        Database database = rBuilder.database();
        if (ObjectUtils.isEmpty(rBuilder) || ObjectUtils.isEmpty(database.url())) {
            throw new RabbiterBuilderException("@Database(url) must be defined in @RBuilder");
        }
    }

    /**
     * 根据注解@RBuilder加载配置信息
     *
     * @param clazz 构造类
     * @throws RabbiterBuilderException rabbiter-builder运行期异常
     */
    default void loadConfigByAnnotation(Class<Object> clazz) throws RabbiterBuilderException {
        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        RBuilder rBuilder = clazz.getAnnotation(RBuilder.class);
        Database database = rBuilder.database();
        if (ObjectUtils.isEmpty(database)) {
            throw new RabbiterBuilderException("The @RBuilder need an annotation : @Database");
        }
        Application application = rBuilder.application();
        if (ObjectUtils.isEmpty(application)) {
            throw new RabbiterBuilderException("The @RBuilder need an annotation : @Application");
        }
        BasePackage basePackage = rBuilder.basePackage();
        if (ObjectUtils.isEmpty(basePackage)) {
            throw new RabbiterBuilderException("The @RBuilder need an annotation : @BasePackage");
        }
        Targets targets = rBuilder.targets();

        /*
         * 封装数据库信息
         */
        configContext.setDriver(database.driver());
        configContext.setUserName(database.username());
        configContext.setPassword(database.password());
        configContext.setUrl(database.url());
        configContext.setDataBaseName(StringUtil.getDataBaseNameFromUrl(database.url()));

        /*
         * 封装应用信息
         */
        configContext.setApplicationName(application.name());
        configContext.setApplicationLargeName(StringUtil.toCamelhump(application.name()));
        configContext.setSpringBootApplicationSuffix(application.springBootApplicationSuffix());

        /*
         * 封装目标包信息
         */
        configContext.setTargetPackage(basePackage.value());
        configContext.setTargetEntityPackage(targets.entity().targetPackage());
        configContext.setTargetServicePackage(targets.service().targetPackage());
        configContext.setTargetServiceImplPackage(targets.serviceImpl().targetPackage());
        configContext.setTargetControllerPackage(targets.controller().targetPackage());
        configContext.setTargetDaoPackage(targets.dao().targetPackage());
        configContext.setTargetDtoPackage(targets.dto().targetPackage());
        configContext.setTargetConfigPackage(targets.config().targetPackage());

        /*
         * 封装目标类文件后缀信息
         */
        configContext.setTargetEntitySuffix(targets.entity().suffix());
        configContext.setTargetServiceSuffix(targets.service().suffix());
        configContext.setTargetServiceImplSuffix(targets.serviceImpl().suffix());
        configContext.setTargetControllerSuffix(targets.controller().suffix());
        configContext.setTargetDaoSuffix(targets.dao().suffix());
        configContext.setTargetDtoSuffix(targets.dto().suffix());
        configContext.setTargetConfigSuffix(targets.config().suffix());

        /*
         * 封装目标配置文件后缀名
         */
        configContext.setTargetMapperSuffix(targets.mapperXml().suffix());
        configContext.setTargetMapperPath(targets.mapperXml().targetPackage());
    }

}
