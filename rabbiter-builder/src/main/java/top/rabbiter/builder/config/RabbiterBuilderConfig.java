package top.rabbiter.builder.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import top.rabbiter.builder.constants.TargetsConstants;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.util.DbUtil;
import top.rabbiter.builder.util.RabbiterYamlUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 配置文件上下文对象，作用范围：一张表
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
@Setter
@Getter
@ToString
public class RabbiterBuilderConfig {
    private static volatile RabbiterBuilderConfig INSTANCE;

    private RabbiterBuilderConfig() {
        try {
            /*
             * 封装本工程加载路径（类加载路径）
             */
            File directory = new File("");
            String projectBasicPath = directory.getCanonicalPath();
            this.setOutputPath(String.format("%s\\rabbiter-builder\\src\\main\\java\\", projectBasicPath));
            this.setSourcePath(String.format("%s\\rabbiter-builder\\src\\main\\resources\\", projectBasicPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
         * 获取外部工程类加载路径（绝对路径）
         */
        String projectBasicPath = RabbiterYamlUtil.class.getResource("/").toString();
        projectBasicPath = projectBasicPath.substring(6, projectBasicPath.lastIndexOf("target/classes/"));
        this.setAbsolutePath(String.format("%ssrc/main/java/", projectBasicPath));
        this.setAbsoluteSourcePath(String.format("%ssrc/main/resources/", projectBasicPath));

        /*
         * 初始化全局忽略列表，默认不生成DTO
         */
        this.ignoreList = new ArrayList<>();
        this.ignoreList.add(TargetsConstants.MARK.DTO);
    }

    /**
     * 单例获取
     *
     * @return 全局配置
     */
    public static synchronized RabbiterBuilderConfig getInstance() {
        if (ObjectUtils.isEmpty(INSTANCE)) {
            synchronized (RabbiterBuilderConfig.class) {
                if (INSTANCE == null) {
                    INSTANCE = new RabbiterBuilderConfig();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * rabbiter-builder的类路径
     */
    private String outputPath;
    /**
     * rabbiter-builder的资源路径
     */
    private String sourcePath;
    /**
     * 生成工程的类绝对路径
     */
    private String absolutePath;
    /**
     * 生成工程的资源绝对路径
     */
    private String absoluteSourcePath;

    /**
     * 项目名称
     */
    private String applicationName;
    /**
     * 项目名称驼峰命名，首字母大写
     */
    private String applicationLargeName;
    /**
     * SpringBoot启动器后缀名
     */
    private String springBootApplicationSuffix;

    /**
     * 数据库连接信息
     */
    private String driver;
    private String url;
    private String userName;
    private String password;
    private String dataBaseName;

    /**
     * 目标包信息
     */
    private String targetPackage;
    private String targetEntityPackage;
    private String targetServicePackage;
    private String targetServiceImplPackage;
    private String targetControllerPackage;
    private String targetDaoPackage;
    private String targetDtoPackage;
    private String targetConfigPackage;

    /**
     * 目标类后缀名信息
     */
    private String targetEntitySuffix;
    private String targetServiceSuffix;
    private String targetServiceImplSuffix;
    private String targetControllerSuffix;
    private String targetDaoSuffix;
    private String targetDtoSuffix;
    private String targetConfigSuffix;

    /**
     * 配置文件信息
     */
    private String targetMapperPath;
    private String targetMapperSuffix;

    /**
     * 所有处理后的表信息
     */
    private List<TableEntity> tableEntityList;

    /**
     * 全局忽视生成的目标
     */
    private List<TargetsConstants.MARK> ignoreList;

    /**
     * 所有表元数据信息
     */
    private Map<String, List<Map<String, Object>>> tablesMetaData;

    /**
     * 获取所有表元数据信息
     *
     * @return 所有表元数据信息
     */
    public Map<String, List<Map<String, Object>>> getTablesMetaData() {
        if (CollectionUtils.isEmpty(INSTANCE.tablesMetaData)) {
            try {
                DbUtil.installTablesMetaData();
            } catch (Exception e) {
                new RabbiterBuilderException(String.format("%s%s", "DB ERROR", e.getMessage())).printStackTrace();
                System.exit(0);
            }
        }
        return INSTANCE.tablesMetaData;
    }
}
