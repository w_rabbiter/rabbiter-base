package top.rabbiter.builder.entity;

import top.rabbiter.builder.util.SqlTypeUtil;
import top.rabbiter.builder.util.StringUtil;
import lombok.Data;

/**
 * 描述：表字段定义实体
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
@Data
public class ColumnDefinition {
    private String columnName;
    private String type;
    private boolean isIdentity;
    private boolean isPk;
    private String defaultValue;
    private String columnComment;

    /**
     * 是否是自增主键
     *
     * @return 是否是自增主键
     */
    public boolean getIsIdentityPk() {
        return isPk && isIdentity;
    }

    /**
     * 返回java字段名,并且第一个字母大写
     *
     * @return 实体属性名，首字母大写
     */
    public String getJavaFieldNameUf() {
        return StringUtil.firstToUpper(getJavaFieldName());
    }

    /**
     * 返回java字段名,并且第一个字母小写
     *
     * @return 实体属性名，首字母小写
     */
    public String getJavaFieldName() {
        return StringUtil.underlineToCamelhump(columnName);
    }

    /**
     * 获得基本类型,int,float
     *
     * @return 基本数据类型
     */

    public String getJavaType() {
        String typeLower = type.toLowerCase();
        return SqlTypeUtil.convertToJavaType(typeLower);
    }

    /**
     * 获得装箱类型,Integer,Float
     *
     * @return 装箱类型
     */

    public String getJavaTypeBox() {
        String typeLower = type.toLowerCase();
        return SqlTypeUtil.convertToJavaBoxType(typeLower);
    }

    public String getMybatisJdbcType() {
        String typeLower = type.toLowerCase();
        return SqlTypeUtil.convertToMyBatisJdbcType(typeLower);
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getIsIdentity() {
        return isIdentity;
    }

    public void setIsIdentity(boolean isIdentity) {
        this.isIdentity = isIdentity;
    }

    public boolean getIsPk() {
        return isPk;
    }

    public void setIsPk(boolean isPk) {
        this.isPk = isPk;
    }
}
