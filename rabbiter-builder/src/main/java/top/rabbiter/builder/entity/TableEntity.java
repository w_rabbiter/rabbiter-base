package top.rabbiter.builder.entity;

import lombok.Data;
import org.springframework.util.ObjectUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.TargetsConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：表-目标实体映射对象
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
@Data
public class TableEntity {
    /**
     * 表注释
     */
    private String tableComment;

    /**
     * 表名（下划线命名法）
     */
    private String tableName;

    /**
     * 类名（驼峰命名法，首字母大写）
     */
    private String targetName;

    /**
     * 类名（驼峰命名法，首字母小写）
     */
    private String targetSmallName;

    /**
     * 生成的DTO名称
     */
    private String dtoName;

    /**
     * 字段定义信息
     */
    private List<ColumnDefinition> columns;

    /**
     * 需要忽略生成的目标信息
     */
    private List<TargetsConstants.MARK> ignoreMarkList;

    /**
     * 重写dtoName获取规则
     *
     * @return dtoName
     */
    public String getDtoName() {
        return ObjectUtils.isEmpty(this.dtoName) ?
                String.format("%s%s", this.targetName, RabbiterBuilderConfig.getInstance().getTargetDtoSuffix())
                : this.dtoName;
    }
}
