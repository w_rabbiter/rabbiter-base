package top.rabbiter.builder.service;

import top.rabbiter.builder.config.RabbiterBuilderConfig;
import org.apache.velocity.VelocityContext;
import top.rabbiter.builder.exception.RabbiterBuilderException;

/**
 * 描述：Velocity函数式接口
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public interface Callback {
    /**
     * 写入文件函数式方法
     *
     * @param context Velocity配置
     * @throws RabbiterBuilderException 框架运行期异常
     */
    void write(VelocityContext context) throws RabbiterBuilderException;
}
