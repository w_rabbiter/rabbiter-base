package top.rabbiter.builder.exception;

/**
 * 构造器运行期异常
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/4
 */
public class RabbiterBuilderException extends Exception {
    public RabbiterBuilderException(String message) {
        super(String.format("RABBITER BUILDER ERROR >>> %s", message));
    }
}
