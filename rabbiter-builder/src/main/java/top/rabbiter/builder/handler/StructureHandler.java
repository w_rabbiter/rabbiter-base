package top.rabbiter.builder.handler;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.service.Callback;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

/**
 * 描述：文件构造处理器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/30
 */
public interface StructureHandler extends FileCreateHandler, TableHandler {
    /**
     * 根据表构建对应类和配置
     *
     * @param tableEntity 表信息实体对象
     * @throws RabbiterBuilderException 框架运行期异常
     */
    default void create(TableEntity tableEntity) throws RabbiterBuilderException {
        //得到数据库所有表的元数据
        doGenerator(tableEntity, (velocityContext) -> {
            //生成一组对应的类文件
            createGroupClassFile(velocityContext, tableEntity);
            //生成单个的文件（如配置类）
            createSingleClassFile(velocityContext);
            //生成application.yml各环境配置文件
            createApplicationYamlFile(velocityContext);
        });
    }

    /**
     * 根据模板生成文件
     *
     * @param tableEntity 表信息实体对象
     * @param callback    生成文件方法函数
     * @throws RabbiterBuilderException 框架运行期异常
     */
    default void doGenerator(TableEntity tableEntity, Callback callback) throws RabbiterBuilderException {
        //获取全局配置
        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        //配置velocity的资源加载路径,，并设置编码
        Properties velocityPros = new Properties();
        velocityPros.setProperty("file.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        velocityPros.setProperty(Velocity.ENCODING_DEFAULT, "UTF-8");
        velocityPros.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
        velocityPros.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");

        Velocity.init(velocityPros);
        //开始封装velocity数据
        VelocityContext velocityContext = new VelocityContext();
        //封装项目基本数据
        velocityContext.put("applicationName", configContext.getApplicationName());
        velocityContext.put("applicationLargeName", configContext.getApplicationLargeName());
        velocityContext.put("springBootApplicationSuffix", configContext.getSpringBootApplicationSuffix());
        //封装数据库信息
        velocityContext.put("driver", configContext.getDriver());
        velocityContext.put("url", configContext.getUrl());
        velocityContext.put("username", configContext.getUserName());
        velocityContext.put("password", configContext.getPassword());
        //封装表信息
        velocityContext.put("tableComment", tableEntity.getTableComment());
        velocityContext.put("tableName", tableEntity.getTableName());
        //封装类名和对象名信息
        velocityContext.put("name", tableEntity.getTargetName());
        velocityContext.put("smallName", tableEntity.getTargetSmallName());
        //封装数据库字段定义与类属性定义信息
        velocityContext.put("columns", tableEntity.getColumns());
        //封装包信息
        velocityContext.put("package", configContext.getTargetPackage());
        velocityContext.put("entityPackage", configContext.getTargetEntityPackage());
        velocityContext.put("servicePackage", configContext.getTargetServicePackage());
        velocityContext.put("serviceImplPackage", configContext.getTargetServiceImplPackage());
        velocityContext.put("controllerPackage", configContext.getTargetControllerPackage());
        velocityContext.put("daoPackage", configContext.getTargetDaoPackage());
        velocityContext.put("dtoPackage", configContext.getTargetDtoPackage());
        velocityContext.put("configPackage", configContext.getTargetConfigPackage());
        //封装目标类后缀名信息
        velocityContext.put("entitySuffix", configContext.getTargetEntitySuffix());
        velocityContext.put("serviceSuffix", configContext.getTargetServiceSuffix());
        velocityContext.put("serviceImplSuffix", configContext.getTargetServiceImplSuffix());
        velocityContext.put("controllerSuffix", configContext.getTargetControllerSuffix());
        velocityContext.put("daoSuffix", configContext.getTargetDaoSuffix());
        velocityContext.put("dtoSuffix", configContext.getTargetDtoSuffix());
        //封装日期信息
        velocityContext.put("date", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE));
        //封装特殊指定名称的类信息
        velocityContext.put("dtoName", tableEntity.getDtoName());
        callback.write(velocityContext);
    }
}
