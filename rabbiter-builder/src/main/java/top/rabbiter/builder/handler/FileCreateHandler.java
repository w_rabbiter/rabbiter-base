package top.rabbiter.builder.handler;

import org.apache.velocity.VelocityContext;
import org.springframework.util.CollectionUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.TargetsConstants;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.util.FileUtil;
import top.rabbiter.builder.util.StringUtil;
import top.rabbiter.builder.util.VelocityUtil;

/**
 * 描述：文件生成处理器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/11/2
 */
public interface FileCreateHandler {

    /**
     * 生成一组相关文件（数量与表数量相关）
     *
     * @param velocityContext Velocity配置
     * @param tableEntity     表-实体映射信息
     */
    default void createGroupClassFile(VelocityContext velocityContext, TableEntity tableEntity) {
        final RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        //生成工程的根目录
        String outputPath = configContext.getAbsolutePath();
        //生成的类文件（基础包）路径
        String rootPath = String.format("%s%s", outputPath, StringUtil.getPathByPackage(configContext.getTargetPackage()));
        //忽略列表是否为空。若为true表示空
        boolean ignoreListEmpty = CollectionUtils.isEmpty(tableEntity.getIgnoreMarkList());
        String entityFileName = String.format("%s.java", tableEntity.getTargetName() + configContext.getTargetEntitySuffix());
        String entityFileFullPath = String.format("%s%s/%s", rootPath, configContext.getTargetEntityPackage(), entityFileName);
        if (!FileUtil.exist(entityFileFullPath) && !configContext.getIgnoreList().contains(TargetsConstants.MARK.ENTITY)) {
            //生成一组基础文件（单位：一张表）
            if (ignoreListEmpty || !tableEntity.getIgnoreMarkList().contains(TargetsConstants.MARK.ENTITY)) {
                FileUtil.writeFile(rootPath + configContext.getTargetEntityPackage(),
                        entityFileName,
                        VelocityUtil.render("entity.vm", velocityContext));
            }
        }

        String daoFileName = String.format("%s%s.java", tableEntity.getTargetName(), configContext.getTargetDaoSuffix());
        String daoFileFullPath = String.format("%s%s/%s", rootPath, configContext.getTargetDaoPackage(), daoFileName);
        if (!FileUtil.exist(daoFileFullPath) && !configContext.getIgnoreList().contains(TargetsConstants.MARK.DAO)) {
            if (ignoreListEmpty || !tableEntity.getIgnoreMarkList().contains(TargetsConstants.MARK.DAO)) {
                FileUtil.writeFile(rootPath + configContext.getTargetDaoPackage(),
                        daoFileName,
                        VelocityUtil.render("dao.vm", velocityContext));
            }
        }

        String serviceFileName = String.format("%s%s.java", tableEntity.getTargetName(), configContext.getTargetServiceSuffix());
        String serviceFileFullPath = String.format("%s%s/%s", rootPath, configContext.getTargetServicePackage(), serviceFileName);
        if (!FileUtil.exist(serviceFileFullPath) && !configContext.getIgnoreList().contains(TargetsConstants.MARK.SERVICE)) {
            if (ignoreListEmpty || !tableEntity.getIgnoreMarkList().contains(TargetsConstants.MARK.SERVICE)) {
                FileUtil.writeFile(rootPath + configContext.getTargetServicePackage(),
                        serviceFileName,
                        VelocityUtil.render("service.vm", velocityContext));
            }
        }

        String serviceImplFileName = String.format("%s%s.java", tableEntity.getTargetName(), configContext.getTargetServiceImplSuffix());
        String serviceImplFileFullPath = String.format("%s%s/%s", rootPath, StringUtil.getPathByPackage(configContext.getTargetServiceImplPackage()), serviceImplFileName);
        if (!FileUtil.exist(serviceImplFileFullPath) && !configContext.getIgnoreList().contains(TargetsConstants.MARK.SERVICE_IMPL)) {
            if (ignoreListEmpty || !tableEntity.getIgnoreMarkList().contains(TargetsConstants.MARK.SERVICE_IMPL)) {
                FileUtil.writeFile(rootPath + StringUtil.getPathByPackage(configContext.getTargetServiceImplPackage()) + "\\",
                        serviceImplFileName,
                        VelocityUtil.render("service_impl.vm", velocityContext));
            }
        }

        String controllerFileName = String.format("%s%s.java", tableEntity.getTargetName(), configContext.getTargetControllerSuffix());
        String controllerFileFullPath = String.format("%s%s/%s", rootPath, configContext.getTargetControllerPackage(), controllerFileName);
        if (!FileUtil.exist(controllerFileFullPath) && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONTROLLER)) {
            if (ignoreListEmpty || !tableEntity.getIgnoreMarkList().contains(TargetsConstants.MARK.CONTROLLER)) {
                FileUtil.writeFile(rootPath + configContext.getTargetControllerPackage(),
                        controllerFileName,
                        VelocityUtil.render("controller.vm", velocityContext));
            }
        }

        String dtoFileName = String.format("%s.java", tableEntity.getDtoName());
        String dtoFileFullPath = String.format("%s%s/%s", rootPath, configContext.getTargetDtoPackage(), dtoFileName);
        if (!FileUtil.exist(dtoFileFullPath) && !configContext.getIgnoreList().contains(TargetsConstants.MARK.DTO)) {
            if (ignoreListEmpty || !tableEntity.getIgnoreMarkList().contains(TargetsConstants.MARK.DTO)) {
                FileUtil.writeFile(rootPath + configContext.getTargetDtoPackage(),
                        dtoFileName,
                        VelocityUtil.render("dto.vm", velocityContext));
            }
        }

        String mapperXmlFileName = String.format("%s%s.xml", tableEntity.getTargetName(), configContext.getTargetMapperSuffix());
        String mapperXmlFileFullPath = String.format("%s/%s/%s", configContext.getAbsoluteSourcePath(), configContext.getTargetMapperPath(), mapperXmlFileName);
        if (!FileUtil.exist(mapperXmlFileFullPath) && !configContext.getIgnoreList().contains(TargetsConstants.MARK.MAPPER_XML)) {
            if (ignoreListEmpty || !tableEntity.getIgnoreMarkList().contains(TargetsConstants.MARK.MAPPER_XML)) {
                FileUtil.writeFile(String.format("%s/%s", configContext.getAbsoluteSourcePath(), configContext.getTargetMapperPath()),
                        mapperXmlFileName,
                        VelocityUtil.render("mapper_xml.vm", velocityContext));
            }
        }
    }

    /**
     * 生成一组特殊类文件（只生成一次的文件）
     *
     * @param velocityContext Velocity配置
     */
    default void createSingleClassFile(VelocityContext velocityContext) {
        final RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        //生成工程的根目录
        String outputPath = configContext.getAbsolutePath();
        //生成的类文件（基础包）路径
        String rootPath = String.format("%s%s", outputPath, StringUtil.getPathByPackage(configContext.getTargetPackage()));
        //生成Swagger2配置类
        String configPath = rootPath + configContext.getTargetConfigPackage();
        String swaggerConfigName = "Swagger2Config.java";
        if (!FileUtil.exist(String.format("%s/%s", configPath, swaggerConfigName))
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.SWAGGER_CONFIG)) {
            //创建Swagger2配置类
            FileUtil.writeFile(configPath, swaggerConfigName, VelocityUtil.render("swagger_config.vm", velocityContext));
        }

        //生成mybatis-plus配置类
        String mybatisPlusConfigName = "MybatisPlusConfig.java";
        if (!FileUtil.exist(String.format("%s/%s", configPath, mybatisPlusConfigName))
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.MYBATIS_PLUS_CONFIG)) {
            //首次创建mybatis-plus配置类
            FileUtil.writeFile(configPath, mybatisPlusConfigName,
                    VelocityUtil.render("mybatis_plus_config.vm", velocityContext));
        }

        //生成Spring boot启动类
        String applicationName =
                String.format("%s%s.java", configContext.getApplicationLargeName(), configContext.getSpringBootApplicationSuffix());
        if (!FileUtil.exist(rootPath + applicationName)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION)) {
            //首次创建Spring boot启动类
            FileUtil.writeFile(rootPath, applicationName, VelocityUtil.render("application.vm", velocityContext));
        }

        //生成接口增强器
        String adviceName = "ApiResponseBodyAdvice.java";
        String aopPath = rootPath + "aop/";
        if (!FileUtil.exist(aopPath)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.ADVICE)) {
            //首次创建Spring boot启动类
            FileUtil.writeFile(aopPath, adviceName, VelocityUtil.render("advice.vm", velocityContext));
        }
    }

    /**
     * 生成各环境application.yml文件
     *
     * @param context Velocity配置
     */
    default void createApplicationYamlFile(VelocityContext context) {
        final RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        String applicationConfigName = "application.yml";
        String resourcesPath = configContext.getAbsoluteSourcePath();
        if (!FileUtil.exist(resourcesPath + applicationConfigName)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION_YML)) {
            //首次创建application.yml配置文件
            FileUtil.writeFile(resourcesPath, applicationConfigName,
                    VelocityUtil.render("application_yml.vm", context));
        }
        String applicationLocalConfigName = "application-local.yml";
        if (!FileUtil.exist(resourcesPath + applicationLocalConfigName)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION_LOCAL_YML)) {
            //首次创建application-local.yml配置文件
            FileUtil.writeFile(resourcesPath, applicationLocalConfigName,
                    VelocityUtil.render("application_local_yml.vm", context));
        }
        String applicationDevConfigName = "application-dev.yml";
        if (!FileUtil.exist(resourcesPath + applicationDevConfigName)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION_DEV_YML)) {
            //首次创建application-dev.yml配置文件
            FileUtil.writeFile(resourcesPath, applicationDevConfigName,
                    VelocityUtil.render("application_dev_yml.vm", context));
        }
        String applicationTestConfigName = "application-test.yml";
        if (!FileUtil.exist(resourcesPath + applicationTestConfigName)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION_TEST_YML)) {
            //首次创建application-test.yml配置文件
            FileUtil.writeFile(resourcesPath, applicationTestConfigName,
                    VelocityUtil.render("application_test_yml.vm", context));
        }
        String applicationStagingConfigName = "application-staging.yml";
        if (!FileUtil.exist(resourcesPath + applicationStagingConfigName)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION_STAGING_YML)) {
            //首次创建application-staging.yml配置文件
            FileUtil.writeFile(resourcesPath, applicationStagingConfigName,
                    VelocityUtil.render("application_staging_yml.vm", context));
        }
        String applicationProdConfigName = "application-prod.yml";
        if (!FileUtil.exist(resourcesPath + applicationProdConfigName)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)
                && !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION_PROD_YML)) {
            //首次创建application-prod.yml配置文件
            FileUtil.writeFile(resourcesPath, applicationProdConfigName,
                    VelocityUtil.render("application_prod_yml.vm", context));
        }
    }
}
