package top.rabbiter.builder.handler;

import top.rabbiter.builder.RabbiterBuilder;
import top.rabbiter.builder.adapter.BuilderAdapter;
import top.rabbiter.builder.annotation.RBuilder;
import top.rabbiter.builder.builder.AnnotationBuilder;
import top.rabbiter.builder.builder.ConfigBuilder;
import top.rabbiter.builder.builder.UiBuilder;
import top.rabbiter.builder.constants.BuilderConstants;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import org.springframework.util.ObjectUtils;

import java.lang.annotation.Annotation;

/**
 * 描述：Rabbiter-builder统一处理器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/28
 */
public abstract class BuilderHandler {
    /**
     * 获取构造器
     *
     * @param clazz RBuilder注解所在类
     * @return 构造器
     * @throws RabbiterBuilderException 框架运行期异常
     */
    protected static BuilderAdapter getBuilder(Class clazz) throws RabbiterBuilderException {
        BuilderAdapter builderAdapter;
        RBuilder rBuilder = (RBuilder) clazz.getAnnotation(RBuilder.class);
        BuilderConstants.TYPE buildAction = rBuilder.value();
        switch (buildAction) {
            case CONFIG:
                builderAdapter = new ConfigBuilder();
                break;
            case ANNOTATION:
                builderAdapter = new AnnotationBuilder();
                break;
            case UI:
                builderAdapter = new UiBuilder();
                break;
            default:
                throw new RabbiterBuilderException(String.format("The BUILD.TYPE : [%s] is not supported", buildAction.getCode()));
        }
        return builderAdapter;
    }

    /**
     * 检查构造类是否包含注解@RBuilder
     *
     * @param clazz 构造类
     * @throws RabbiterBuilderException 框架运行期异常
     * @see RBuilder
     */
    protected static void checkClazz(Class clazz) throws RabbiterBuilderException {
        Annotation rBuilder = clazz.getAnnotation(RBuilder.class);
        if (ObjectUtils.isEmpty(rBuilder)) {
            String errorMsg = String.format("The Class : %s need an annotation : @RBuilder", clazz.getName());
            throw new RabbiterBuilderException(errorMsg);
        }
    }
}
