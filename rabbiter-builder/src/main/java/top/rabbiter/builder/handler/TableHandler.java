package top.rabbiter.builder.handler;

import org.springframework.util.CollectionUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.SqlConstants;
import top.rabbiter.builder.entity.ColumnDefinition;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.util.ColumnUtil;
import top.rabbiter.builder.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 描述：表处理器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/30
 */
public interface TableHandler {
    /**
     * 从数据库加载表信息到上下文
     *
     * @throws RabbiterBuilderException 框架运行期异常
     */
    default void loadTableEntity() throws RabbiterBuilderException {
        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        List<TableEntity> tableEntityList = new ArrayList<>();
        try {
            //获取所有表与字段映射
            Map<String, List<ColumnDefinition>> map
                    = ColumnUtil.covertColumnDefinition(configContext.getTablesMetaData());
            for (String key : map.keySet()) {
                if (CollectionUtils.isEmpty(map.get(key))) {
                    //表字段为空，不创建对应类文件
                    continue;
                }
                TableEntity tableEntity = new TableEntity();
                //封装表名
                tableEntity.setTableName(key);
                //封装表注释
                String tableComment =
                        (String) configContext.getTablesMetaData().get(key)
                                .get(0).get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.TABLE_COMMENT.getField());
                tableEntity.setTableComment(tableComment);
                //封装表字段数据
                tableEntity.setColumns(map.get(key));
                //封装类名，转驼峰命名，首字母大写
                String targetName = StringUtil.underlineToCamelhump(key);
                targetName = StringUtil.firstToUpper(targetName);
                tableEntity.setTargetName(targetName);
                //封装类名引用，转驼峰命名，首字母小写
                String targetSmallName = StringUtil.firstToLower(targetName);
                tableEntity.setTargetSmallName(targetSmallName);
                //加入列表
                tableEntityList.add(tableEntity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RabbiterBuilderException(e.getMessage());
        }
        //将解析之后的表信息存入上下文
        configContext.setTableEntityList(tableEntityList);
    }
}
