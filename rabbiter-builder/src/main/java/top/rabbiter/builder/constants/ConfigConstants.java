package top.rabbiter.builder.constants;

/**
 * 配置文件键常量
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public enum ConfigConstants {
    //项目工程配置
    APPLICATION_NAME("rabbiter.application.name"),

    //jdbc驱动包
    DB_DRIVER("rabbiter.jdbc.driver"),
    // 数据库链接
    DB_URL("rabbiter.jdbc.url"),
    //数据库用户名
    DB_USERNAME("rabbiter.jdbc.username"),
    //数据库密码
    DB_PASSWORD("rabbiter.jdbc.password"),

    //基础包父路径
    PK_BASE("rabbiter.target.base"),

    //实体包子路径
    PK_ENTITY("rabbiter.target.entity.package"),
    //实体类名后缀
    SUFFIX_ENTITY("rabbiter.target.entity.suffix"),

    //业务接口包子路径
    PK_SERVICE("rabbiter.target.service.package"),
    //业务接口名后缀
    SUFFIX_SERVICE("rabbiter.target.service.suffix"),

    //业务实现包子路径
    PK_SERVICE_IMPL("rabbiter.target.serviceImpl.package"),
    //业务实现类名后缀
    SUFFIX_SERVICE_IMPL("rabbiter.target.serviceImpl.suffix"),

    //控制器包子路径
    PK_CONTROLLER("rabbiter.target.controller.package"),
    //控制器类名后缀
    SUFFIX_CONTROLLER("rabbiter.target.controller.suffix"),

    //DAO层包子路径
    PK_DAO("rabbiter.target.dao.package"),
    //DAO接口名后缀
    SUFFIX_DAO("rabbiter.target.dao.suffix"),

    //传输数据模型包子路径
    PK_DTO("rabbiter.target.dto.package"),
    //传输数据模型类名后缀
    SUFFIX_DTO("rabbiter.target.dto.suffix"),

    //配置包路径
    PK_CONFIG("rabbiter.target.config.package");

    private final String key;

    ConfigConstants(String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }
}
