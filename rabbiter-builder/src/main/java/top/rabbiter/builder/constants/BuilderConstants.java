package top.rabbiter.builder.constants;

/**
 * 描述：构造器常量
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/25
 */
public interface BuilderConstants {

    enum TYPE {
        /**
         * 构造方式常量
         */
        CONFIG(Byte.parseByte("1"), "以配置文件方式启动构造器"),
        ANNOTATION(Byte.parseByte("1"), "以注解方式启动构造器"),
        UI(Byte.parseByte("1"), "以UI界面方式启动构造器");
        private Byte code;

        private String description;

        TYPE(Byte code, String description) {
            this.code = code;
            this.description = description;
        }

        public Byte getCode() {
            return code;
        }

        public void setCode(Byte code) {
            this.code = code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
