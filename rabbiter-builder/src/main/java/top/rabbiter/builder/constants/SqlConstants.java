package top.rabbiter.builder.constants;

/**
 * 描述：sql语句常量
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/11/4
 */
public interface SqlConstants {
    enum INFORMATION_SCHEMA_COLUMN_NAME {
        /**
         * mysql 的information_schema表字段名称枚举
         */
        DATABASE_NAME("TABLE_SCHEMA", "数据库名称"),
        TABLE_NAME("TABLE_NAME", "表名"),
        COLUMN_NAME("COLUMN_NAME", "字段名"),
        COLUMN_INDEX("ORDINAL_POSITION", "字段在表中位置，1为起始下标"),
        COLUMN_DEFAULT("COLUMN_DEFAULT", "字段默认值"),
        COLUMN_ISNULL("IS_NULLABLE", "字段是否可以为NULL"),
        COLUMN_TYPE("DATA_TYPE", "字段数据类型。例：varchar"),
        COLUMN_FULL_TYPE("COLUMN_TYPE", "字段类型。例：varchar(255)"),
        COLUMN_KEY("COLUMN_KEY", "字段键信息。PRI主键"),
        COLUMN_COMMENT("COLUMN_COMMENT", "字段注释"),
        COLUMN_EXTRA("EXTRA", "字段额外信息"),
        TABLE_COMMENT("TABLE_COMMENT", "表注释");
        private String field;
        private String description;

        INFORMATION_SCHEMA_COLUMN_NAME(String field, String description) {
            this.field = field;
            this.description = description;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
