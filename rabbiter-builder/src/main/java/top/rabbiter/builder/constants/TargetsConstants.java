package top.rabbiter.builder.constants;

import org.springframework.util.ObjectUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 描述：生成目标相关常量
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/27
 */
public interface TargetsConstants {
    /**
     * 生成目标标识常量
     */
    enum MARK {
        /**
         * 生成目标标识常量
         */
        ENTITY("实体类"),
        DTO("数据传输模型"),
        DAO("数据访问层"),
        SERVICE("业务逻辑接口"),
        SERVICE_IMPL("业务逻辑类"),
        CONTROLLER("控制类"),
        CONFIG("配置类"),
        MAPPER_XML("Mybatis映射文件"),
        SWAGGER_CONFIG("SwaggerConfig配置文件"),
        MYBATIS_PLUS_CONFIG("Mybatis-Plus配置文件"),
        ADVICE("公共报文响应增强类"),
        APPLICATION_DEV_YML("application-dev.yml文件"),
        APPLICATION_PROD_YML("application-prod.yml文件"),
        APPLICATION_STAGING_YML("application-staging.yml文件"),
        APPLICATION_TEST_YML("application-test.yml文件"),
        APPLICATION_LOCAL_YML("application-local.yml文件"),
        APPLICATION_YML("application.yml文件"),
        APPLICATION("SpringBoot启动器");

        private final String name;

        MARK(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        /**
         * 从配置上下文获取生成目标类的后缀名
         *
         * @param configContext 配置上下文
         * @return 后缀名
         */
        public String getTargetSuffixFromConfigContext(RabbiterBuilderConfig configContext) {
            switch (this) {
                case ENTITY:
                    return ObjectUtils.isEmpty(configContext.getTargetEntitySuffix()) ? "" : configContext.getTargetEntitySuffix();
                case DAO:
                    return ObjectUtils.isEmpty(configContext.getTargetDaoSuffix()) ? "" : configContext.getTargetDaoSuffix();
                case DTO:
                    return ObjectUtils.isEmpty(configContext.getTargetDtoSuffix()) ? "" : configContext.getTargetDtoSuffix();
                case SERVICE:
                    return ObjectUtils.isEmpty(configContext.getTargetServiceSuffix()) ? "" : configContext.getTargetServiceSuffix();
                case SERVICE_IMPL:
                    return ObjectUtils.isEmpty(configContext.getTargetServiceImplSuffix()) ? "" : configContext.getTargetServiceImplSuffix();
                case CONTROLLER:
                    return ObjectUtils.isEmpty(configContext.getTargetControllerSuffix()) ? "" : configContext.getTargetControllerSuffix();
                case CONFIG:
                    return ObjectUtils.isEmpty(configContext.getTargetConfigSuffix()) ? "" : configContext.getTargetConfigSuffix();
                default:
                    return "";
            }
        }

        /**
         * 返回一个mark全列表
         *
         * @return mark全列表
         */
        public static List<MARK> getAllList() {
            List<MARK> allMarkList = new ArrayList<>();
            Collections.addAll(allMarkList, MARK.values());
            return allMarkList;
        }
    }
}
