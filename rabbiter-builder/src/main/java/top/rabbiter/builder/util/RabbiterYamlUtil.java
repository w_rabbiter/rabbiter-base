package top.rabbiter.builder.util;

import top.rabbiter.builder.constants.ConfigConstants;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import org.springframework.util.ObjectUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 读取rabbiter_builder.yml配置文件的工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class RabbiterYamlUtil {

    private static Map<String, Map<String, Object>> properties = new HashMap<>();

    /**
     * 单例
     */
    public static final RabbiterYamlUtil INSTANCE = new RabbiterYamlUtil();

    static {
        Yaml yaml = new Yaml();
        try {
            InputStream in = RabbiterYamlUtil.class.getClassLoader().getResourceAsStream("rabbiter-builder.yml");
            if (in == null) {
                throw new RabbiterBuilderException("Please create rabbiter-builder.yml for your project");
            }
            properties = yaml.load(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据key获取yaml文件的值
     * <p>
     * 可能返回字符串或者Map
     *
     * @param key 键
     * @return 返回字符串或者Map
     * @throws RabbiterBuilderException 框架运行期异常
     */
    @SuppressWarnings("unchecked")
    public String getValueByKey(String key) throws RabbiterBuilderException {
        if (properties == null) {
            throw new RabbiterBuilderException("Please edit the rabbiter-builder.yml correctly");
        }
        String separator = ".";
        String[] separatorKeys;
        if (key.contains(separator)) {
            separatorKeys = key.split("\\.");
        } else {
            return String.valueOf(properties.get(key));
        }
        Map<String, Object> finalValue = new HashMap<>();
        for (int i = 0; i < separatorKeys.length - 1; i++) {
            if (i == 0) {
                finalValue = properties.get(separatorKeys[i]);
                continue;
            }
            if (finalValue == null) {
                break;
            }
            finalValue = (Map<String, Object>) finalValue.get(separatorKeys[i]);
        }
        if (finalValue == null) {
            throw new RabbiterBuilderException(String.format("Please add configuration: [%s]", key));
        }
        Object value = finalValue.get(separatorKeys[separatorKeys.length - 1]);
        if (ObjectUtils.isEmpty(value)) {
            if (!key.equals(ConfigConstants.SUFFIX_ENTITY.key())) {
                //除了entity的后缀可以传入空串，其他均不允许
                throw new RabbiterBuilderException(String.format("The configuration : [%s] need defined a value, and you can't set ''", key));
            } else {
                //entity的后缀名可以省略
                return "";
            }
        }
        return String.valueOf(value);
    }
}
