package top.rabbiter.builder.util;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.StringWriter;

/**
 * 描述：Velocity工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class VelocityUtil {
    /**
     * 根据模板内容生成文件
     *
     * @param vm      模板路径
     * @param context Velocity配置信息
     * @return 文件内容
     */
    public static String render(String vm, VelocityContext context) {
        String content = "";

        Template template;
        try {
            template = Velocity.getTemplate(vm);
            StringWriter writer = new StringWriter();
            if (template != null) {
                template.merge(context, writer);
            }
            writer.flush();
            writer.close();
            content = writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content;
    }

}
