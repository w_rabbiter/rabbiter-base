package top.rabbiter.builder.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 描述：文件工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class FileUtil {
    /**
     * 将字符串写入文件
     * @param filePath 文件路径
     * @param fileName 文件名
     * @param fileContent 文件内容
     */
    public static void writeFile(String filePath, String fileName, String fileContent) {
        File f = new File(filePath);
        if (!f.exists()) {
            f.mkdirs();
        }
        File myFile = new File(f, fileName);
        FileWriter w = null;
        try {
            w = new FileWriter(myFile);
            w.write(fileContent);
        } catch (IOException e) {
            throw new RuntimeException("Error creating file " + fileName, e);
        } finally {
            if (w != null) {
                try {
                    w.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    /**
     * 判断文件是否存在
     *
     * @param filePth 文件路径
     * @return 是否存在
     */
    public static Boolean exist(String filePth) {
        File file = new File(filePth);
        return file.exists();
    }


    /**
     * 删除单个文件
     *
     * @param sPath 被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String sPath) {
        boolean flag = false;
        File file = new File(sPath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            flag = file.delete();
        }
        return flag;
    }
}
