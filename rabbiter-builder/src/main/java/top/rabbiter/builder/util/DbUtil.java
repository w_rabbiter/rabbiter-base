package top.rabbiter.builder.util;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.SqlConstants;
import top.rabbiter.builder.exception.RabbiterBuilderException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：数据库工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class DbUtil {

    public static Connection conn;

    private DbUtil() {

    }

    /**
     * 初始化数据库
     *
     * @return 数据源
     */
    private static DataSource initDataSource() {
        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(configContext.getDriver());
        dataSource.setUrl(configContext.getUrl());
        dataSource.setUsername(configContext.getUserName());
        dataSource.setPassword(configContext.getPassword());
        try {
            conn = dataSource.getConnection();
        } catch (SQLException e) {
            new RabbiterBuilderException(e.getMessage()).printStackTrace();
            System.exit(0);
        }
        return dataSource;
    }

    /**
     * 初始化全局配置中的表元数据
     * <p>
     * 查询数据库所有表结构
     * <p>
     * 从mysql的 INFORMATION_SCHEMA.`COLUMNS` 表和 INFORMATION_SCHEMA.`TABLES` 表获取
     */
    public static void installTablesMetaData() {
        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        String sql = String.format("SELECT\n" +
                "*\n" +
                "FROM\n" +
                "INFORMATION_SCHEMA.`COLUMNS` c\n" +
                "INNER JOIN\n" +
                "INFORMATION_SCHEMA.`TABLES` t\n" +
                "ON\n" +
                "c.TABLE_NAME = t.TABLE_NAME\n" +
                "WHERE\n" +
                "c.TABLE_SCHEMA = '%s'\n" +
                "AND\n" +
                "t.TABLE_SCHEMA = '%s'", configContext.getDataBaseName(), configContext.getDataBaseName());
        List<Map<String, Object>> mapList = queryMapList(sql, (Object[]) null);
        //存放 表:字段列表 数据map
        Map<String, List<Map<String, Object>>> tablesMetaData = new HashMap<>();
        //存放当前遍历到的表名
        String tableName =
                CollectionUtils.isEmpty(mapList) ? "" : (String) mapList.get(0).get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.TABLE_NAME.getField());
        //存放当前遍历到表的字段集
        List<Map<String, Object>> columns = new ArrayList<>();
        //遍历所有表字段集合
        for (Map<String, Object> map : mapList) {
            //获取当前遍历到的表名
            String currentTableName = (String) map.get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.TABLE_NAME.getField());
            if (!currentTableName.equals(tableName)) {
                //存入一张表信息
                tablesMetaData.put(tableName, columns);
                //遍历到当前属性所属表名更换，切换表名与初始化字段集合
                tableName = currentTableName;
                columns = new ArrayList<>();
            }
            columns.add(map);
        }
        //存入最后一张表的元数据
        tablesMetaData.put(tableName, columns);

        /*
         * 初始化元数据
         */
        configContext.setTablesMetaData(tablesMetaData);
    }

    /**
     * 查询
     *
     * @param sql    sql语句
     * @param params 参数
     * @return 结果集
     */
    public static List<Map<String, Object>> queryMapList(String sql, Object... params) {
        DataSource dataSource = initDataSource();
        if(ObjectUtils.isEmpty(dataSource)) {
            return new ArrayList<>();
        }
        List<Map<String, Object>> fieldMapList;
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            fieldMapList = queryRunner.query(sql, new MapListHandler(), params);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return fieldMapList;
    }
}
