package top.rabbiter.builder.util;

import org.apache.commons.lang.StringUtils;
import top.rabbiter.builder.constants.SqlConstants;
import top.rabbiter.builder.entity.ColumnDefinition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：表字段工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class ColumnUtil {
    /**
     * 从所有表元数据中提取字段定义
     *
     * @param tablesMetaData 所有表元数据
     * @return 表:字段定义列表 映射
     */
    public static Map<String, List<ColumnDefinition>> covertColumnDefinition(Map<String, List<Map<String, Object>>> tablesMetaData) {
        Map<String, List<ColumnDefinition>> tablesColumnDefinition = new HashMap<>(tablesMetaData.size());
        //开始遍历每张表
        for (String key : tablesMetaData.keySet()) {
            //开始遍历每个字段
            List<ColumnDefinition> columnDefinitionList = new ArrayList<>();
            for (Map<String, Object> column : tablesMetaData.get(key)) {
                //推入每个字段定义信息
                columnDefinitionList.add(buildColumnDefinition(column));
            }
            //将 表:字段定义列表 存入结果
            tablesColumnDefinition.put(key, columnDefinitionList);
        }
        return tablesColumnDefinition;
    }

    public static ColumnDefinition buildColumnDefinition(Map<String, Object> columnMap) {
        //DBUtils 取的MapList中的Map  是不区分key大小写的  所以不用转换
        ColumnDefinition columnDefinition = new ColumnDefinition();
        //列名
        columnDefinition.setColumnName(
                (String) columnMap.get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.COLUMN_NAME.getField())
        );
        //额外信息（是否自增）
        boolean isIdentity = "auto_increment".equalsIgnoreCase(
                (String) columnMap.get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.COLUMN_EXTRA.getField())
        );
        columnDefinition.setIsIdentity(isIdentity);
        //是否主键
        boolean isPk = "PRI".equalsIgnoreCase(
                (String) columnMap.get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.COLUMN_KEY.getField())
        );
        columnDefinition.setIsPk(isPk);
        //字段类型
        String type = buildType((String) columnMap.get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.COLUMN_TYPE.getField()));
        columnDefinition.setType(type);
        //默认值
        String defaultValue = (String) columnMap.get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.COLUMN_DEFAULT.getField());
        columnDefinition.setDefaultValue(defaultValue);
        //列注释
        String comment = (String) columnMap.get(SqlConstants.INFORMATION_SCHEMA_COLUMN_NAME.COLUMN_COMMENT.getField());
        columnDefinition.setColumnComment(comment);
        return columnDefinition;
    }

    public static String buildType(String type) {
        if (StringUtils.isNotEmpty(type)) {
            int index = type.indexOf("(");
            if (index > 0) {
                return type.substring(0, index).toUpperCase();
            }
            return type;
        }
        return "varchar";
    }
}
