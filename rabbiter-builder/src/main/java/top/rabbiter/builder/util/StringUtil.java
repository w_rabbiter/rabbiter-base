package top.rabbiter.builder.util;

import top.rabbiter.builder.RabbiterBuilder;
import top.rabbiter.builder.exception.RabbiterBuilderException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * 字符串工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class StringUtil {
    private static final Pattern UNDERLINE_PATTERN = Pattern.compile("_[a-z]");
    private static final Pattern MIDDLE_LINE_PATTERN = Pattern.compile("-[a-z]");

    /**
     * 将下划线风格替换为驼峰风格
     *
     * @param str 下划线风格字符串
     * @return 驼峰风格字符串
     */
    public static String underlineToCamelhump(String str) {
        if (isNotEmpty(str)) {
            Matcher matcher = UNDERLINE_PATTERN.matcher(str);
            StringBuilder builder = new StringBuilder(str);
            for (int i = 0; matcher.find(); i++) {
                builder.replace(matcher.start() - i, matcher.end() - i, matcher.group().substring(1).toUpperCase());
            }
            if (Character.isUpperCase(builder.charAt(0))) {
                builder.replace(0, 1, String.valueOf(Character.toLowerCase(builder.charAt(0))));
            }
            return builder.toString();
        }
        return "";
    }

    /**
     * 将中划线风格替换为驼峰风格
     *
     * @param str 中划线风格字符串
     * @return 驼峰风格字符串
     */
    public static String middleLineToCamelhump(String str) {
        if (isNotEmpty(str)) {
            Matcher matcher = MIDDLE_LINE_PATTERN.matcher(str);
            StringBuilder builder = new StringBuilder(str);
            for (int i = 0; matcher.find(); i++) {
                builder.replace(matcher.start() - i, matcher.end() - i, matcher.group().substring(1).toUpperCase());
            }
            if (Character.isUpperCase(builder.charAt(0))) {
                builder.replace(0, 1, String.valueOf(Character.toLowerCase(builder.charAt(0))));
            }
            return builder.toString();
        }
        return "";
    }

    /**
     * 将字符串首字母大写
     *
     * @param str 字符串
     * @return 首字母大写字符串
     */
    public static String firstToUpper(String str) {
        if (isNotEmpty(str)) {
            if (str.length() > 1) {
                return Character.toUpperCase(str.charAt(0)) + str.substring(1);
            } else {
                return Character.toUpperCase(str.charAt(0)) + "";
            }
        }
        return "";
    }

    /**
     * 将字符串首字母小写
     *
     * @param str 字符串
     * @return 首字母小写字符串
     */
    public static String firstToLower(String str) {
        return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }

    /**
     * 将包路径转化为文件路径
     *
     * @param packagePath 包路径
     * @return 文件路径
     */
    public static String getPathByPackage(String packagePath) {
        return packagePath.replace(".", "/") + "/";
    }

    /**
     * 从url中提取数据库名称
     *
     * @param url 数据库链接
     * @return 数据库名称
     * @throws RabbiterBuilderException url解析异常
     */
    @SuppressWarnings("all")
    public static String getDataBaseNameFromUrl(String url) throws RabbiterBuilderException {
        String bias = "/";
        String quesMark = "?";
        try {
            String lastStringUrl = url.split(bias)[3];
            if (lastStringUrl.contains(quesMark)) {
                lastStringUrl = lastStringUrl.substring(0, lastStringUrl.indexOf(quesMark));
            }
            return lastStringUrl;
        } catch (Exception e) {
            throw new RabbiterBuilderException(String.format("Invalid Database Url : %s", url));
        }
    }

    /**
     * 转驼峰命名法（所有中横线和下划线都会被转化）
     *
     * @param s 非驼峰命名法字符串
     * @return 驼峰命名法字符串
     */
    public static String toCamelhump(String s) {
        //先将下划线转驼峰命名
        String camelhump = underlineToCamelhump(s);
        //将中划线转转驼峰命名
        camelhump = middleLineToCamelhump(camelhump);
        //将首字母大写
        return firstToUpper(camelhump);
    }
}
