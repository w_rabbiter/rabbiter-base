package top.rabbiter.builder.builder;

import top.rabbiter.builder.adapter.BuilderAdapter;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.ConfigConstants;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.executor.RabbiterBuilderExecutor;
import top.rabbiter.builder.handler.PrintHandler;
import top.rabbiter.builder.util.RabbiterYamlUtil;
import top.rabbiter.builder.util.StringUtil;

/**
 * 描述：配置文件构造器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/25
 */
public class ConfigBuilder implements BuilderAdapter, PrintHandler {
    @Override
    public void run(Class clazz) {
        try {
            buildStart();
            //加载yaml配置文件到上下文
            loadConfigByYml();
            //启动执行器
            new RabbiterBuilderExecutor().execute();
            //构造完成
            buildSuccess();
        } catch (RabbiterBuilderException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据yml配置加载配置信息
     *
     * @throws RabbiterBuilderException rabbiter-builder运行期异常
     */
    private void loadConfigByYml() throws RabbiterBuilderException {
        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();

        /*
         * 封装项目基本信息
         */
        String applicationName = RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.APPLICATION_NAME.key());
        configContext.setApplicationName(applicationName);
        configContext.setApplicationLargeName(StringUtil.toCamelhump(applicationName));

        /*
         * 封装数据库信息
         */
        configContext.setDriver(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.DB_DRIVER.key()));
        configContext.setUserName(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.DB_USERNAME.key()));
        configContext.setPassword(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.DB_PASSWORD.key()));
        String url = RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.DB_URL.key());
        configContext.setUrl(url);
        configContext.setDataBaseName(StringUtil.getDataBaseNameFromUrl(url));

        /*
         * 封装目标包信息
         */
        configContext.setTargetPackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_BASE.key()));
        configContext.setTargetEntityPackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_ENTITY.key()));
        configContext.setTargetServicePackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_SERVICE.key()));
        configContext.setTargetServiceImplPackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_SERVICE_IMPL.key()));
        configContext.setTargetControllerPackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_CONTROLLER.key()));
        configContext.setTargetDaoPackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_DAO.key()));
        configContext.setTargetDtoPackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_DTO.key()));
        configContext.setTargetConfigPackage(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.PK_CONFIG.key()));

        /*
         * 封装目标类文件后缀信息
         */
        configContext.setTargetEntitySuffix(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.SUFFIX_ENTITY.key()));
        configContext.setTargetServiceSuffix(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.SUFFIX_SERVICE.key()));
        configContext.setTargetServiceImplSuffix(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.SUFFIX_SERVICE_IMPL.key()));
        configContext.setTargetControllerSuffix(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.SUFFIX_CONTROLLER.key()));
        configContext.setTargetDaoSuffix(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.SUFFIX_DAO.key()));
        configContext.setTargetDtoSuffix(RabbiterYamlUtil.INSTANCE.getValueByKey(ConfigConstants.SUFFIX_DTO.key()));

        /*
         * 封装不可配置的默认配置
         */
        configContext.setSpringBootApplicationSuffix("Application");
        configContext.setTargetMapperSuffix("Mapper");
        configContext.setTargetMapperPath("mappers");
    }
}
