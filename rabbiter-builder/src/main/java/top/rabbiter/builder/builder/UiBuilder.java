package top.rabbiter.builder.builder;

import org.springframework.util.CollectionUtils;
import top.rabbiter.builder.adapter.BuilderAdapter;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.TargetsConstants;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.handler.PrintHandler;
import top.rabbiter.builder.handler.TableHandler;
import top.rabbiter.builder.ui.frame.MainMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 描述：UI界面构造器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/25
 */
public class UiBuilder implements BuilderAdapter, TableHandler, PrintHandler {
    @Override
    public void run(Class clazz) {
        try {
            buildStart();
            checkDatabaseAnnotation(clazz);

            //读取@RBuilder的配置信息到配置上下文
            loadConfigByAnnotation(clazz);

            //从数据库读取所有表信息到上下文
            loadTableEntity();

            //初始化忽视生成文件的列表（默认类文件均不生成）
            RabbiterBuilderConfig.getInstance().setIgnoreList(TargetsConstants.MARK.getAllList());
            for (TableEntity tableEntity : RabbiterBuilderConfig.getInstance().getTableEntityList()) {
                List<TargetsConstants.MARK> ignoreMarkList = new ArrayList<>();
                ignoreMarkList.add(TargetsConstants.MARK.DTO);
                tableEntity.setIgnoreMarkList(ignoreMarkList);
            }

            //构造界面
            new MainMenu().init();
        } catch (RabbiterBuilderException e) {
            e.printStackTrace();
        }
    }
}
