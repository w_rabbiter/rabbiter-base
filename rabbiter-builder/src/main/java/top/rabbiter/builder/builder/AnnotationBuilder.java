package top.rabbiter.builder.builder;

import org.springframework.util.ObjectUtils;
import top.rabbiter.builder.adapter.BuilderAdapter;
import top.rabbiter.builder.annotation.*;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.executor.RabbiterBuilderExecutor;
import top.rabbiter.builder.handler.PrintHandler;
import top.rabbiter.builder.util.StringUtil;

/**
 * 描述：注解构造器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/25
 */
public class AnnotationBuilder implements BuilderAdapter, PrintHandler {
    @Override
    public void run(Class clazz) {
        try {
            buildStart();
            //检查@Database注解完整性
            checkDatabaseAnnotation(clazz);
            //读取@RBuilder的配置信息到配置上下文
            loadConfigByAnnotation(clazz);
            //启动执行器
            new RabbiterBuilderExecutor().execute();
            //构造完成
            buildSuccess();
        } catch (RabbiterBuilderException e) {
            e.printStackTrace();
        }
    }

}
