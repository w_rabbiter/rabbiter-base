package top.rabbiter.builder.test;

import top.rabbiter.builder.RabbiterBuilder;
import top.rabbiter.builder.annotation.*;
import top.rabbiter.builder.constants.BuilderConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

/**
 * @author WuZhengHua
 * @version 1.0
 * @since 2020/9/28
 */
//@RBuilder(
//        value = BuilderConstants.TYPE.ANNOTATION,
//        application = @Application(name = "rabbiter-demo", springBootApplicationSuffix = "Starter"),
//        database = @Database(
//                driver = "com.mysql.jdbc.Driver",
//                url = "jdbc:mysql://localhost:3306/rabbiter_v2?serverTimezone=Asia/Shanghai&characterEncoding=UTF8&useSSL=false",
//                username = "root",
//                password = "root"
//        ),
//        basePackage = @BasePackage("com.rabbiter.demo"),
//        targets = @Targets(
//                entity = @Target(targetPackage = "entity", suffix = ""),
//                dto = @Target(targetPackage = "dto", suffix = "DTO"),
//                dao = @Target(targetPackage = "mapper", suffix = "Mapper"),
//                service = @Target(targetPackage = "service", suffix = "Service"),
//                serviceImpl = @Target(targetPackage = "service.impl", suffix = "ServiceImpl"),
//                controller = @Target(targetPackage = "rpc", suffix = "RpcImpl"),
//                config = @Target(targetPackage = "config", suffix = "Config"),
//                mapperXml = @Target(targetPackage = "mapper", suffix = "_mapper")
//        )
//)
//@RBuilder(value = BuilderConstants.TYPE.UI,
//        database = @Database(url = "jdbc:mysql://localhost:3306/rabbiter_v2?serverTimezone=Asia/Shanghai&characterEncoding=UTF8&useSSL=false"))
@RBuilder(BuilderConstants.TYPE.CONFIG)
public class Builder {
    public static void main(String[] args) {
        RabbiterBuilder.run(Builder.class);
    }
}