package top.rabbiter.builder.annotation;

import top.rabbiter.builder.constants.BuilderConstants;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author WuZhengHua
 * @version 1.0
 * @since 2020/9/28
 */
@Documented
@java.lang.annotation.Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RBuilder {
    /**
     * 构造方式
     *
     * @return 构造方式
     * @see top.rabbiter.builder.constants.BuilderConstants.TYPE
     */
    BuilderConstants.TYPE value();

    /**
     * 应用信息
     *
     * @return 应用信息
     */
    Application application() default @Application;

    /**
     * 基础包信息
     *
     * @return 基础包信息
     */
    BasePackage basePackage() default @BasePackage;

    /**
     * 数据源信息
     *
     * @return 数据源信息
     */
    Database database() default @Database;

    /**
     * 生成的目标类信息
     *
     * @return 生成的目标类信息
     */
    Targets targets() default @Targets();
}
