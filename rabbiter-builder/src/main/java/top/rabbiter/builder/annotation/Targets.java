package top.rabbiter.builder.annotation;

import java.lang.annotation.*;

/**
 * 描述：生成的目标类文件信息
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/28
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.ANNOTATION_TYPE})
public @interface Targets {
    /**
     * 生成的目标实体类信息
     *
     * @return 生成的目标实体类信息
     */
    top.rabbiter.builder.annotation.Target entity() default @top.rabbiter.builder.annotation.Target(targetPackage = "entity", suffix = "DO");

    /**
     * 生成的目标控制类信息
     *
     * @return 生成的目标控制类信息
     */
    top.rabbiter.builder.annotation.Target dto() default @top.rabbiter.builder.annotation.Target(targetPackage = "dto", suffix = "DTO");

    /**
     * 生成的目标DAO类信息
     *
     * @return 生成的目标DAO类信息
     */
    top.rabbiter.builder.annotation.Target dao() default @top.rabbiter.builder.annotation.Target(targetPackage = "dao", suffix = "DAO");

    /**
     * 生成的目标业务接口信息
     *
     * @return 生成的目标业务接口信息
     */
    top.rabbiter.builder.annotation.Target service() default @top.rabbiter.builder.annotation.Target(targetPackage = "service", suffix = "Service");

    /**
     * 生成的目标业务类信息
     *
     * @return 生成的目标业务类信息
     */
    top.rabbiter.builder.annotation.Target serviceImpl() default @top.rabbiter.builder.annotation.Target(targetPackage = "service.impl", suffix = "ServiceImpl");

    /**
     * 生成的目标控制类信息
     *
     * @return 生成的目标控制类信息
     */
    top.rabbiter.builder.annotation.Target controller() default @top.rabbiter.builder.annotation.Target(targetPackage = "controller", suffix = "Controller");

    /**
     * 生成的目标配置类信息
     *
     * @return 生成的目标配置类信息
     */
    top.rabbiter.builder.annotation.Target config() default @top.rabbiter.builder.annotation.Target(targetPackage = "config", suffix = "Config");

    /**
     * 生成的目标mapper.xml配置文件信息
     *
     * @return 目标mapper.xml配置文件信息
     */
    top.rabbiter.builder.annotation.Target mapperXml() default @top.rabbiter.builder.annotation.Target(targetPackage = "mappers", suffix = "Mapper");

}
