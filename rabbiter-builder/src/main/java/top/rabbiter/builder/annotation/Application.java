package top.rabbiter.builder.annotation;

import java.lang.annotation.*;

/**
 * 描述：配置构造应用信息
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/28
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.ANNOTATION_TYPE})
public @interface Application {
    /**
     * 生成的项目名称
     *
     * @return 项目名称
     */
    String name() default "rabbiter-builder-demo";

    /**
     * SpringBoot启动器后缀名
     *
     * @return SpringBoot启动器后缀名
     */
    String springBootApplicationSuffix() default "Application";

}
