package top.rabbiter.builder.annotation;

import java.lang.annotation.*;

/**
 * 描述：构造项目的数据来源
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/28
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.ANNOTATION_TYPE})
public @interface Database {
    /**
     * 数据库驱动
     *
     * @return 驱动包位置
     */
    String driver() default "com.mysql.cj.jdbc.Driver";

    /**
     * 数据库连接
     *
     * @return 连接url
     */
    String url() default "";

    /**
     * 数据库访问用户名
     *
     * @return 用户名
     */
    String username() default "root";

    /**
     * 数据库访问密码
     *
     * @return 数据库访问密码
     */
    String password() default "root";
}
