package top.rabbiter.builder.annotation;

import java.lang.annotation.*;

/**
 * 描述：生成目标类名称和包名
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/28
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.ANNOTATION_TYPE})
public @interface Target {
    /**
     * 目标类的包名
     * @return 目标类的包名
     */
    String targetPackage();

    /**
     * 目标类的后缀名
     * @return 目标类的后缀名
     */
    String suffix();
}
