package top.rabbiter.builder.annotation;

import java.lang.annotation.*;

/**
 * 描述：生成的基础包名称
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/28
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.ANNOTATION_TYPE})
public @interface BasePackage {
    /**
     * 基础包名
     *
     * @return 基础包名
     */
    String value() default "com.rabbiter.demo";
}
