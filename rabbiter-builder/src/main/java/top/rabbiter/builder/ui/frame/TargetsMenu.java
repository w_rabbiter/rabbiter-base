package top.rabbiter.builder.ui.frame;

import org.springframework.util.CollectionUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.TargetsConstants;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.ui.entity.CheckBoxForm;
import top.rabbiter.builder.ui.entity.TextForm;
import top.rabbiter.builder.ui.panel.BaseComponent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：生成目标确认窗口
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/27
 */
public class TargetsMenu extends JFrame implements BaseComponent {
    private final JPanel mainPanel;
    private final TargetsConstants.MARK mark;
    private final TextForm parentForm;
    /**
     * 需要忽视生成文件对应的实体名称
     */
    private final List<String> ignoreTargetNameList;

    /**
     * 封装textField标记。Key为targetName
     */
    private final Map<String, JTextField> textFieldMap;

    public TargetsMenu(TargetsConstants.MARK mark, TextForm parentForm) throws HeadlessException {
        super(String.format("生成【%s】配置(Bate)", mark.getName()));
        this.mark = mark;
        this.mainPanel = new JPanel();
        this.parentForm = parentForm;
        this.ignoreTargetNameList = new ArrayList<>();
        this.textFieldMap = new HashMap<>();
        init();
    }

    public void init() {
        this.setResizable(false);
        this.setBackground(Color.WHITE);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
        addComponents();
        this.setSize(450, 600);
        this.setLocationRelativeTo(null);
    }

    public void addComponents() {
        this.add(mainPanel, BorderLayout.CENTER);
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        //滚动条面板
        JScrollPane sp=new JScrollPane(mainPanel);
        this.getContentPane().add(sp);

        RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();
        List<TableEntity> tableEntityList = configContext.getTableEntityList();

        for (TableEntity tableEntity : tableEntityList) {
            List<TargetsConstants.MARK> ignoreMarkList = tableEntity.getIgnoreMarkList();
            //是否选中复选框
            boolean checkIt = CollectionUtils.isEmpty(ignoreMarkList) || !ignoreMarkList.contains(mark);
            if (!checkIt) {
                //没有选中，添加进忽视列表
                ignoreTargetNameList.add(tableEntity.getTargetName());
            }
            CheckBoxForm checkBoxForm = new CheckBoxForm(checkIt, getCheckBoxActionListener(tableEntity), tableEntity.getDtoName());
            mainPanel.add(createCheckBoxForm(checkBoxForm));
            //缓存textField
            textFieldMap.put(tableEntity.getTargetName(), checkBoxForm.getTextField());
        }

        mainPanel.setLayout(new GridLayout(mainPanel.getComponentCount(), 1));
        JButton confirm = new JButton("确认");
        confirm.addActionListener(createConfirmActionListener(tableEntityList));
        this.add(confirm, BorderLayout.SOUTH);
    }

    /**
     * 为复选框添加监听
     *
     * @param tableEntity 表-实体映射
     * @return 监听
     */
    private ActionListener getCheckBoxActionListener(TableEntity tableEntity) {
        return e -> {
            if (ignoreTargetNameList.contains(tableEntity.getTargetName())) {
                ignoreTargetNameList.remove(tableEntity.getTargetName());
            } else {
                if (!ignoreTargetNameList.contains(tableEntity.getTargetName())) {
                    ignoreTargetNameList.add(tableEntity.getTargetName());
                }
            }
        };
    }

    /**
     * 为确认按钮添加监听
     *
     * @return 监听
     */
    private ActionListener createConfirmActionListener(List<TableEntity> tableEntityList) {
        return e -> {
            parentForm.getJTextField().setEditable(false);
            parentForm.getShowButton().setText(String.format("自定义(%s)", tableEntityList.size() - ignoreTargetNameList.size()));

            //处理多选框逻辑 和 处理文本框逻辑
            for (TableEntity tableEntity : tableEntityList) {
                //处理多选框逻辑
                if (!CollectionUtils.isEmpty(ignoreTargetNameList)) {
                    //操作复选框
                    if (ignoreTargetNameList.contains(tableEntity.getTargetName())) {
                        //取消勾选，代表不生成文件。添加进忽视列表
                        if (!tableEntity.getIgnoreMarkList().contains(mark)) {
                            tableEntity.getIgnoreMarkList().add(mark);
                        }
                    } else {
                        //勾选，代表需要生成文件。从忽视列表中移除
                        tableEntity.getIgnoreMarkList().remove(mark);
                    }
                } else {
                    tableEntity.getIgnoreMarkList().remove(mark);
                }

                //处理文本框逻辑
                if (!CollectionUtils.isEmpty(textFieldMap)) {
                    JTextField textField = textFieldMap.get(tableEntity.getTargetName());
                    if (mark.equals(TargetsConstants.MARK.DTO)) {
                        //重新设置DTO名称
                        tableEntity.setDtoName(textField.getText());
                    }
                }
            }
            this.dispose();
        };
    }
}
