package top.rabbiter.builder.ui.entity;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * 描述：单一复选框表单实体
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/27
 */
@Getter
@Setter
public class CheckBoxForm {
    /**
     * 是否选中复选框
     */
    private Boolean sel;

    /**
     * 复选框
     */
    private JCheckBox checkBox;

    /**
     * 复选框监听
     */
    private ActionListener checkBoxActionListener;

    /**
     * 文本框默认值
     */
    private String value;

    /**
     * 文本框
     */
    private JTextField textField;

    public CheckBoxForm(Boolean sel, ActionListener checkBoxActionListener, String value) {
        this.sel = sel;
        this.checkBoxActionListener = checkBoxActionListener;
        this.value = value;
    }
}
