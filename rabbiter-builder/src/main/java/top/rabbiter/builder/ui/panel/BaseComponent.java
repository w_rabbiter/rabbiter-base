package top.rabbiter.builder.ui.panel;

import org.springframework.util.ObjectUtils;
import top.rabbiter.builder.ui.entity.CheckBoxForm;
import top.rabbiter.builder.ui.entity.TextForm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * 描述：公共组件
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/26
 */
public interface BaseComponent {
    /**
     * 创建文本框表单
     *
     * @param textForm 渲染表单数据
     * @return 面板
     */
    default JPanel createTextForm(TextForm textForm) {
        JPanel textFormPanel = new JPanel();
        if (ObjectUtils.isEmpty(textForm)) {
            return textFormPanel;
        }
        textFormPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        //渲染checkbox
        if (!ObjectUtils.isEmpty(textForm.getSel())) {
            JCheckBox checkBox = new JCheckBox("", textForm.getSel());
            textFormPanel.add(checkBox);
            //添加监听
            checkBox.addActionListener(textForm.getCheckBoxActionListener());
        }

        //渲染label
        if (!ObjectUtils.isEmpty(textForm.getLabel())) {
            JLabel nameLabel = new JLabel(textForm.getLabel());
            textFormPanel.add(nameLabel);
        }

        //渲染text
        JTextField text = new JTextField(ObjectUtils.isEmpty(textForm.getDefaultText()) ? "" : textForm.getDefaultText(), 30);
        textForm.setJTextField(text);
        textFormPanel.add(text);
        text.setEditable(ObjectUtils.isEmpty(textForm.getEditText() ? null : textForm.getEditText()));

        //渲染button
        if (!ObjectUtils.isEmpty(textForm.getButtonActionListener())) {
            JButton showButton = new JButton("自定义(0)");
            textForm.setShowButton(showButton);
            textFormPanel.add(showButton);
            //添加监听器
            showButton.addActionListener(textForm.getButtonActionListener());
        }
        return textFormPanel;
    }

    /**
     * 创建单一复选框表单
     *
     * @param checkBoxForm 单一复选框表单实体
     * @return 表单组件
     */
    default JPanel createCheckBoxForm(CheckBoxForm checkBoxForm) {
        JPanel form = new JPanel();
        if (!ObjectUtils.isEmpty(checkBoxForm.getSel())) {
            //多选框
            JCheckBox checkBox = new JCheckBox("", checkBoxForm.getSel());
            checkBoxForm.setCheckBox(checkBox);
            form.add(checkBox);
            //多选框监听
            if (!ObjectUtils.isEmpty(checkBoxForm.getCheckBoxActionListener())) {
                checkBox.addActionListener(checkBoxForm.getCheckBoxActionListener());
            }
        }
        //文本框
        JTextField textField = new JTextField(checkBoxForm.getValue(), 30);
        checkBoxForm.setTextField(textField);
        form.add(textField);
        return form;
    }
}
