package top.rabbiter.builder.ui.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * 描述：text类型表单实体
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/27
 */
@Setter
@Getter
@Accessors(chain = true)
public class TextForm {
    /**
     * 是否选中复选框。
     */
    private Boolean sel;

    /**
     * 复选框监听器
     */
    private ActionListener checkBoxActionListener;

    /**
     * 表单标签
     */
    private String label;

    /**
     * 文本框默认值
     */
    private String defaultText;

    /**
     * 文本框是否可编辑
     */
    private Boolean editText;

    /**
     * 按钮
     */
    private JButton showButton;

    /**
     * 按钮监听器
     */
    private ActionListener buttonActionListener;

    /**
     * 文本框
     */
    private JTextField jTextField;

    public TextForm(Boolean sel, String label, String defaultText, Boolean editText) {
        this.sel = sel;
        this.label = label;
        this.defaultText = defaultText;
        this.editText = editText;
    }
}
