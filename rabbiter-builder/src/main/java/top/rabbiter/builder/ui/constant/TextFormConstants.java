package top.rabbiter.builder.ui.constant;

import lombok.Getter;

/**
 * 描述：表单相关常量
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2021/1/2
 */
public interface TextFormConstants {

    @Getter
    enum MARK {
        /**
         * 表单标识常量
         */
        APPLICATION_NAME("应用名称"),
        BASE_PACKAGE("基础包名"),
        SPRING_BOOT_APPLICATION("SpringBoot启动器名称"),
        CONFIG_PACKAGE("配置类包名"),
        CONFIG_SUFFIX("配置类后缀名"),
        CONTROLLER_PACKAGE("控制层包名"),
        CONTROLLER_SUFFIX("控制层后缀名"),
        SERVICE_IMPL_PACKAGE("业务类包名"),
        SERVICE_IMPL_SUFFIX("业务类后缀名"),
        SERVICE_PACKAGE("业务接口包名"),
        SERVICE_SUFFIX("业务接口后缀名"),
        MAPPER_XML_SUFFIX("mapper.xml文件后缀名"),
        DAO_PACKAGE("数据访问层包名"),
        DAO_SUFFIX("数据访问层后缀名"),
        DTO_PACKAGE("数据传输模型包名"),
        ENTITY_PACKAGE("实体类包名"),
        ENTITY_SUFFIX("实体类后缀名");
        private final String mark;

        MARK(String mark) {
            this.mark = mark;
        }
    }

}
