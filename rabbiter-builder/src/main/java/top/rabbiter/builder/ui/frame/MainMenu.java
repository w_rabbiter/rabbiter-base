package top.rabbiter.builder.ui.frame;

import org.springframework.util.CollectionUtils;
import top.rabbiter.builder.config.RabbiterBuilderConfig;
import top.rabbiter.builder.constants.TargetsConstants;
import top.rabbiter.builder.entity.TableEntity;
import top.rabbiter.builder.exception.RabbiterBuilderException;
import top.rabbiter.builder.handler.PrintHandler;
import top.rabbiter.builder.handler.StructureHandler;
import top.rabbiter.builder.ui.constant.TextFormConstants;
import top.rabbiter.builder.ui.entity.TextForm;
import top.rabbiter.builder.ui.panel.BaseComponent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述：UI-主菜单
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/12/26
 */
public class MainMenu extends JFrame implements BaseComponent, PrintHandler, StructureHandler {
    private final JPanel mainPanel;
    private final JLabel headLabel;
    private final JButton buildButton;
    private final RabbiterBuilderConfig configContext = RabbiterBuilderConfig.getInstance();

    private final Map<TextFormConstants.MARK, TextForm> textFormMap;


    public MainMenu() throws HeadlessException {
        super("Rabbiter-Builder(Bate)");
        this.mainPanel = new JPanel();
        this.headLabel = new JLabel("配置构造信息", SwingConstants.CENTER);
        this.buildButton = new JButton("执行构造器");
        this.textFormMap = new HashMap<>(32, 1);
    }

    public void init() {
        this.headLabel.setFont(new Font("微软雅黑", Font.BOLD, 20));
        this.setResizable(false);
        this.setBackground(Color.WHITE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        addComponents();
        this.setSize(650, 900);
        this.setLocationRelativeTo(null);

        //将初始化文件移除
        configContext.getIgnoreList().remove(TargetsConstants.MARK.MYBATIS_PLUS_CONFIG);
        configContext.getIgnoreList().remove(TargetsConstants.MARK.SWAGGER_CONFIG);
        configContext.getIgnoreList().remove(TargetsConstants.MARK.APPLICATION_PROD_YML);
        configContext.getIgnoreList().remove(TargetsConstants.MARK.APPLICATION_DEV_YML);
        configContext.getIgnoreList().remove(TargetsConstants.MARK.APPLICATION_STAGING_YML);
        configContext.getIgnoreList().remove(TargetsConstants.MARK.APPLICATION_TEST_YML);
        configContext.getIgnoreList().remove(TargetsConstants.MARK.APPLICATION_LOCAL_YML);
        configContext.getIgnoreList().remove(TargetsConstants.MARK.APPLICATION_YML);
    }

    public void addComponents() {
        this.add(mainPanel, BorderLayout.CENTER);
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        mainPanel.add(headLabel);

        JLabel applicationConfigLabel = new JLabel("【应用配置】");
        applicationConfigLabel.setFont(new Font("微软雅黑", Font.BOLD, 15));
        mainPanel.add(applicationConfigLabel);
        TextForm applicationNameForm = new TextForm(null, "应用名称", "rabbiter-builder-demo", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.APPLICATION_NAME, applicationNameForm);
        mainPanel.add(createTextForm(applicationNameForm));

        JLabel packageConfigLabel = new JLabel("【包配置】");
        packageConfigLabel.setFont(new Font("微软雅黑", Font.BOLD, 15));
        mainPanel.add(packageConfigLabel);
        TextForm basePackageForm = new TextForm(null, "基础包名", "com.rabbiter.demo", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.BASE_PACKAGE, basePackageForm);
        mainPanel.add(createTextForm(basePackageForm));

        JLabel targetsConfigLabel = new JLabel("【生成目标配置】");
        targetsConfigLabel.setFont(new Font("微软雅黑", Font.BOLD, 15));
        mainPanel.add(targetsConfigLabel);

        addSpringBootApplicationComponent();
        addEntityComponent();
        addDtoComponent();
        addDaoComponent();
        addMapperXmlComponent();
        addServiceComponent();
        addServiceImplComponent();
        addControllerComponent();
        addConfigComponent();

        mainPanel.setLayout(new GridLayout(mainPanel.getComponentCount(), 1));

        this.add(buildButton, BorderLayout.SOUTH);
        buildButton.addActionListener(
                createBuildButtonListener()
        );
    }

    /**
     * 生成构造按钮监听
     *
     * @return 构造按钮监听
     */
    private ActionListener createBuildButtonListener() {
        return actionEvent -> {
            try {
                //初始化在UI的配置
                configContext.setApplicationName(textFormMap.get(TextFormConstants.MARK.APPLICATION_NAME).getJTextField().getText());
                configContext.setTargetPackage(textFormMap.get(TextFormConstants.MARK.BASE_PACKAGE).getJTextField().getText());
                configContext.setApplicationLargeName(textFormMap.get(TextFormConstants.MARK.SPRING_BOOT_APPLICATION).getJTextField().getText());
                configContext.setSpringBootApplicationSuffix("");
                configContext.setTargetEntityPackage(textFormMap.get(TextFormConstants.MARK.ENTITY_PACKAGE).getJTextField().getText());
                configContext.setTargetEntitySuffix(textFormMap.get(TextFormConstants.MARK.ENTITY_SUFFIX).getJTextField().getText());

                configContext.setTargetDtoPackage(textFormMap.get(TextFormConstants.MARK.DTO_PACKAGE).getJTextField().getText());
                configContext.setTargetDaoPackage(textFormMap.get(TextFormConstants.MARK.DAO_PACKAGE).getJTextField().getText());
                configContext.setTargetDaoSuffix(textFormMap.get(TextFormConstants.MARK.DAO_SUFFIX).getJTextField().getText());
                configContext.setTargetServicePackage(textFormMap.get(TextFormConstants.MARK.SERVICE_PACKAGE).getJTextField().getText());
                configContext.setTargetServiceSuffix(textFormMap.get(TextFormConstants.MARK.SERVICE_SUFFIX).getJTextField().getText());
                configContext.setTargetServiceImplPackage(textFormMap.get(TextFormConstants.MARK.SERVICE_IMPL_PACKAGE).getJTextField().getText());
                configContext.setTargetServiceImplSuffix(textFormMap.get(TextFormConstants.MARK.SERVICE_IMPL_SUFFIX).getJTextField().getText());
                configContext.setTargetControllerPackage(textFormMap.get(TextFormConstants.MARK.CONTROLLER_PACKAGE).getJTextField().getText());
                configContext.setTargetControllerSuffix(textFormMap.get(TextFormConstants.MARK.CONTROLLER_SUFFIX).getJTextField().getText());
                configContext.setTargetConfigPackage(textFormMap.get(TextFormConstants.MARK.CONFIG_PACKAGE).getJTextField().getText());
                configContext.setTargetConfigSuffix(textFormMap.get(TextFormConstants.MARK.CONFIG_SUFFIX).getJTextField().getText());
                configContext.setTargetMapperSuffix(textFormMap.get(TextFormConstants.MARK.MAPPER_XML_SUFFIX).getJTextField().getText());

                //开始构建工程
                List<TableEntity> tableEntityList = configContext.getTableEntityList();
                if (!CollectionUtils.isEmpty(tableEntityList)) {
                    for (TableEntity tableEntity : tableEntityList) {
                        create(tableEntity);
                    }
                }
                //构造完成
                buildSuccess();
            } catch (RabbiterBuilderException e) {
                e.printStackTrace();
                System.exit(0);
            }
        };
    }

    /**
     * 渲染SpringBoot启动器表单
     */
    private void addSpringBootApplicationComponent() {
        JLabel label = new JLabel("SpringBoot启动器配置");
        label.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(label);
        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION);
        TextForm textForm = new TextForm(ifSel, "SpringBoot启动器名称",
                String.format("%s%s", configContext.getApplicationLargeName(), "Application"), true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.SPRING_BOOT_APPLICATION, textForm);
        textForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.APPLICATION)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.APPLICATION);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.APPLICATION);
                    }
                }
        );
        mainPanel.add(createTextForm(textForm));
    }

    /**
     * 渲染配置类表单
     */
    private void addConfigComponent() {
        JLabel configLabel = new JLabel("Config");
        configLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(configLabel);
        TextForm configPackForm = new TextForm(null, "配置类包名", "config", true);
        mainPanel.add(createTextForm(configPackForm));
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.CONFIG_PACKAGE, configPackForm);
        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG);
        TextForm textForm = new TextForm(ifSel, "配置类后缀名", "Config", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.CONFIG_SUFFIX, textForm);
        textForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.CONFIG)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.CONFIG);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.CONFIG);
                    }
                }
        );
        mainPanel.add(createTextForm(textForm));
    }

    /**
     * 渲染控制层表单
     */
    private void addControllerComponent() {
        JLabel controllerLabel = new JLabel("Controller");
        controllerLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(controllerLabel);
        TextForm controllerPackForm = new TextForm(null, "控制层包名", "controller", true);
        mainPanel.add(createTextForm(controllerPackForm));
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.CONTROLLER_PACKAGE, controllerPackForm);
        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.CONTROLLER);
        TextForm controllerSuffixForm = new TextForm(ifSel, "控制层后缀名", "Controller", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.CONTROLLER_SUFFIX, controllerSuffixForm);
        controllerSuffixForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.CONTROLLER)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.CONTROLLER);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.CONTROLLER);
                    }
                }
        );
        mainPanel.add(createTextForm(controllerSuffixForm));
    }

    /**
     * 渲染业务逻辑类表单
     */
    private void addServiceImplComponent() {
        JLabel serviceImplLabel = new JLabel("ServiceImpl");
        serviceImplLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(serviceImplLabel);
        TextForm serviceImplPackForm = new TextForm(null, "业务逻辑类包名", "service.impl", true);
        mainPanel.add(createTextForm(serviceImplPackForm));
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.SERVICE_IMPL_PACKAGE, serviceImplPackForm);
        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.SERVICE_IMPL);
        TextForm serviceImplSuffixForm = new TextForm(ifSel, "业务逻辑类后缀名", "ServiceImpl", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.SERVICE_IMPL_SUFFIX, serviceImplSuffixForm);
        serviceImplSuffixForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.SERVICE_IMPL)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.SERVICE_IMPL);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.SERVICE_IMPL);
                    }
                }
        );
        mainPanel.add(createTextForm(serviceImplSuffixForm));
    }

    /**
     * 渲染业务逻辑接口表单
     */
    private void addServiceComponent() {
        JLabel serviceLabel = new JLabel("Service");
        serviceLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(serviceLabel);
        TextForm servicePackForm = new TextForm(null, "业务逻辑接口包名", "service", true);
        mainPanel.add(createTextForm(servicePackForm));
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.SERVICE_PACKAGE, servicePackForm);

        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.SERVICE);
        TextForm serviceSuffixForm = new TextForm(ifSel, "业务逻辑接口后缀名", "Service", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.SERVICE_SUFFIX, serviceSuffixForm);
        serviceSuffixForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.SERVICE)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.SERVICE);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.SERVICE);
                    }
                }
        );
        mainPanel.add(createTextForm(serviceSuffixForm));
    }

    /**
     * 渲染mapper.xml配置表单
     */
    private void addMapperXmlComponent() {
        JLabel mapperXmlLabel = new JLabel("Mapper.xml");
        mapperXmlLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(mapperXmlLabel);

        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.MAPPER_XML);
        TextForm textForm = new TextForm(ifSel, "mapper.xml文件后缀名", "Mapper", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.MAPPER_XML_SUFFIX, textForm);
        textForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.MAPPER_XML)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.MAPPER_XML);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.MAPPER_XML);
                    }
                }
        );
        mainPanel.add(createTextForm(textForm));
    }

    /**
     * 渲染数据访问层表单
     */
    private void addDaoComponent() {
        JLabel daoLabel = new JLabel("DAO");
        daoLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(daoLabel);
        TextForm daoPackForm = new TextForm(null, "数据访问层包名", "dao", true);
        mainPanel.add(createTextForm(daoPackForm));
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.DAO_PACKAGE, daoPackForm);

        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.DAO);
        TextForm daoSuffixForm = new TextForm(ifSel, "数据访问层后缀名", "DAO", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.DAO_SUFFIX, daoSuffixForm);
        daoSuffixForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.DAO)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.DAO);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.DAO);
                    }
                }
        );
        mainPanel.add(createTextForm(daoSuffixForm));
    }

    /**
     * 渲染数据传输模型表单
     */
    private void addDtoComponent() {
        JLabel dtoLabel = new JLabel("DTO");
        dtoLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(dtoLabel);
        TextForm dtoPackForm = new TextForm(null, "数据传输模型包名", "dto", true);
        mainPanel.add(createTextForm(dtoPackForm));
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.DTO_PACKAGE, dtoPackForm);
        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.DTO);
        TextForm dtoSuffixForm = new TextForm(ifSel, "数据传输模型后缀名", "仅支持自定义", false);
        dtoSuffixForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.DTO)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.DTO);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.DTO);
                    }
                }
        );
        dtoSuffixForm.setButtonActionListener(e -> new TargetsMenu(TargetsConstants.MARK.DTO, dtoSuffixForm));
        mainPanel.add(createTextForm(dtoSuffixForm));
    }

    /**
     * 渲染实体类表单
     */
    private void addEntityComponent() {
        JLabel entityLabel = new JLabel("Entity");
        entityLabel.setFont(new Font("微软雅黑", Font.BOLD, 12));
        mainPanel.add(entityLabel);
        TextForm entityPackForm = new TextForm(null, "实体类包名", "entity", true);
        mainPanel.add(createTextForm(entityPackForm));
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.ENTITY_PACKAGE, entityPackForm);
        boolean ifSel = !configContext.getIgnoreList().contains(TargetsConstants.MARK.ENTITY);
        TextForm entitySuffixForm = new TextForm(ifSel, "实体类后缀名", "DO", true);
        //缓存进map
        textFormMap.put(TextFormConstants.MARK.ENTITY_SUFFIX, entitySuffixForm);
        entitySuffixForm.setCheckBoxActionListener(
                e -> {
                    if (configContext.getIgnoreList().contains(TargetsConstants.MARK.ENTITY)) {
                        //勾选
                        configContext.getIgnoreList().remove(TargetsConstants.MARK.ENTITY);
                    } else {
                        //取消勾选
                        configContext.getIgnoreList().add(TargetsConstants.MARK.ENTITY);
                    }
                }
        );
        mainPanel.add(createTextForm(entitySuffixForm));
    }
}
