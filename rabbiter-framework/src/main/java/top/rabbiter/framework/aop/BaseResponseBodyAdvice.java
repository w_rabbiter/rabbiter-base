package top.rabbiter.framework.aop;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import top.rabbiter.framework.annotation.IgnoreBaseResponse;
import top.rabbiter.framework.api.BaseApiResult;
import top.rabbiter.framework.constant.ApiCode;
import top.rabbiter.framework.exception.ApiException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 统一报文增强器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/11
 */
public abstract class BaseResponseBodyAdvice implements ResponseBodyAdvice<Object> {
    /**
     * 被@IgnoreBaseResponse标注的控制类或者方法，不会对其接口响应值进行包装
     *
     * @param methodParameter 方法信息
     * @param aClass          类信息
     * @return 是否进行包装
     * @see IgnoreBaseResponse
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return ObjectUtils.isEmpty(methodParameter.getMethodAnnotation(IgnoreBaseResponse.class))
                && ObjectUtils.isEmpty(methodParameter.getDeclaringClass().getAnnotation(IgnoreBaseResponse.class));
    }

    /**
     * 封装统一报文
     *
     * @param body               body
     * @param methodParameter    methodParameter
     * @param mediaType          mediaType
     * @param aClass             aClass
     * @param serverHttpRequest  serverHttpRequest
     * @param serverHttpResponse serverHttpResponse
     * @return Object
     */
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (body instanceof BaseApiResult) {
            return body;
        } else if (body instanceof String) {
            BaseApiResult.success(body);
            return JSON.toJSONString(BaseApiResult.success(body), SerializerFeature.WriteNullStringAsEmpty);
        } else {
            return BaseApiResult.success(body);
        }
    }

    /**
     * 注入公共控制层日志切面类
     *
     * @return 公共控制层日志切面类
     */
    @Bean
    public BaseControllerLog baseControllerLog() {
        return new BaseControllerLog();
    }

    /**
     * MethodArgumentNotValidException、BindException 异常捕获
     *
     * @param exception MethodArgumentNotValidException或BindException异常
     * @return BaseApiResult报文
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public BaseApiResult bindException(Exception exception) {
        BindingResult bindingResult;
        if (exception instanceof MethodArgumentNotValidException) {
            //json数据异常
            MethodArgumentNotValidException methodArgumentNotValidException = (MethodArgumentNotValidException) exception;
            bindingResult = methodArgumentNotValidException.getBindingResult();
        } else {
            //绑定数据异常（表单）
            BindException bindException = (BindException) exception;
            bindingResult = bindException.getBindingResult();
        }
        StringBuilder errorMessage = new StringBuilder();
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errorMessage.append("[").append(fieldError.getDefaultMessage()).append("] ");
        }
        errorMessage.delete(errorMessage.length() - 1, errorMessage.length());
        return BaseApiResult.setResult(ApiCode.CODE_A0400.code(), null, errorMessage.toString());
    }

    /**
     * IllegalArgumentException异常捕获
     *
     * @param exception IllegalArgumentException异常
     * @return BaseApiResult报文
     */
    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public BaseApiResult illegalArgumentException(Exception exception) {
        IllegalArgumentException illegalArgumentException = (IllegalArgumentException) exception;
        String errorMsg = illegalArgumentException.getMessage();
        return BaseApiResult.setResult(ApiCode.CODE_A0400.code(), null, errorMsg);
    }

    /**
     * 接口统一异常ApiException处理
     * <p>
     * 接口抛出ApiException异常时，统一报文返回值
     *
     * @param exception 接口异常
     * @return 封装接口响应体
     * @see ApiException
     */
    @ExceptionHandler(ApiException.class)
    @ResponseBody
    public ResponseEntity<Object> handleMsSysException(ApiException exception) {
        return new ResponseEntity<>(
                BaseApiResult.setResult(exception.getCode(), null, exception.getMessage()),
                HttpStatus.OK
        );
    }

    /**
     * 配置接口返回参数对象中若存在null的属性直接过滤，不进行返回
     *
     * @param builder Jackson2ObjectMapperBuilder
     * @return ObjectMapper
     */
    @Bean
    @Primary
    @ConditionalOnMissingBean({ObjectMapper.class})
    public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder) {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper;
    }
}
