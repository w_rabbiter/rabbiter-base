package top.rabbiter.framework.aop;

import top.rabbiter.framework.annotation.IgnoreBaseResponse;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 控制层统一日志切面类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/12
 */
@Aspect
@Component
public class BaseControllerLog {
    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 从被@GetMapping标注的方法切入警告日志
     *
     * @param joinPoint  切点
     * @param getMapping GetMapping注解
     * @throws NoSuchMethodException 运行期方法未找到异常
     */
    @Before("@annotation(getMapping)")
    public void logStartAsGetMapping(JoinPoint joinPoint, GetMapping getMapping) throws NoSuchMethodException {
        methodIfReturnString(joinPoint, getMapping);
    }

    /**
     * 从被@PostMapping标注的方法切入警告日志
     *
     * @param joinPoint   切点
     * @param postMapping PostMapping注解
     * @throws NoSuchMethodException 运行期方法未找到异常
     */
    @Before("@annotation(postMapping)")
    public void logStartAsPostMapping(JoinPoint joinPoint, PostMapping postMapping) throws NoSuchMethodException {
        methodIfReturnString(joinPoint, postMapping);
    }

    /**
     * 从被@RequestMapping标注的方法切入警告日志
     *
     * @param joinPoint      切点
     * @param requestMapping RequestMapping注解
     * @throws NoSuchMethodException 运行期方法未找到异常
     */
    @Before("@annotation(requestMapping)")
    public void logStartAsRequestMapping(JoinPoint joinPoint, RequestMapping requestMapping) throws NoSuchMethodException {
        methodIfReturnString(joinPoint, requestMapping);
    }

    /**
     * 如果方法返回值为String类型
     * <p>
     * 并且被BaseResponseBodyAdvice所增强（封装）
     * <p>
     * 这里进行一个警告日志打印
     *
     * @param joinPoint 切点
     * @param obj       注解（目前可能时@GetMapping、@PostMapping、@RequestMapping）
     * @throws NoSuchMethodException 运行期方法未找到异常
     */
    private void methodIfReturnString(JoinPoint joinPoint, Object obj) throws NoSuchMethodException {
        //获取加载类
        Class clazz = joinPoint.getTarget().getClass();
        Annotation annotation = clazz.getAnnotation(IgnoreBaseResponse.class);
        //获取方法名
        String methodName = joinPoint.getSignature().getName();
        Class[] parameterTypes = ((MethodSignature) joinPoint.getSignature()).getMethod().getParameterTypes();
        //获取方法定义
        Method method = clazz.getMethod(methodName, parameterTypes);
        //判断返回值是否为String，若不为String类型直接放行
        String typeName = method.getReturnType().getTypeName();
        String stringName = "java.lang.String";
        if (!stringName.equals(typeName)) {
            return;
        }
        if (!ObjectUtils.isEmpty(annotation) ||
                !ObjectUtils.isEmpty(method.getAnnotation(IgnoreBaseResponse.class))) {
            //若类或方法上被@IgnoreBaseResponse标注，即没有经过BaseResponseBodyAdvice报文包装，则不会打印警告
            return;
        }
        /*
         * 若方法和类上没有被@IgnoreBaseResponse标注，且返回值为null，则打印警告
         */
        printWarnLog(joinPoint);
    }

    /**
     * 打印警告日志
     *
     * @param joinPoint 切入点
     */
    private void printWarnLog(JoinPoint joinPoint) {
        String msg = String.format(
                "Rabbiter Framework Warning >>> The %s is defined and the return value is String , you can add @IgnoreBaseResponse on your method or class",
                joinPoint.getSignature()
        );
        log.warn(msg);
    }
}
