package top.rabbiter.framework.provider;

import com.baomidou.mybatisplus.annotation.TableName;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.exception.RabbiterFrameworkException;
import top.rabbiter.framework.util.ReflectionUtil;
import top.rabbiter.framework.util.StringUtil;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Map;

/**
 * 描述：SQL语句构造器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/14
 */
public class BaseProvider {
    /**
     * 构造批量插入数据sql，注意：不会自动填充属性
     *
     * @param map 构造参数
     * @return sql语句
     * @throws RabbiterFrameworkException 框架运行期异常
     * @throws IllegalAccessException     非法权限异常
     */
    public String batchInsert(Map<String, Object> map) throws RabbiterFrameworkException, IllegalAccessException {
        //待插入实体数据集合
        Collection<Object> collection = (Collection<Object>) map.get("collection");
        //插入数量
        Integer batchSize = (Integer) map.get("batchSize");
        if (collection.isEmpty()) {
            return "";
        }
        //获取表名
        String tableName = getTableNameFromClazz(map);
        //步骤:使用反射机制加载所有属性
        SQL sql = new SQL() {
            {
                INSERT_INTO(tableName);
                VALUES(ReflectionUtil.batchInsertColumns(collection),
                        ReflectionUtil.batchInsertValues(collection, batchSize));
            }
        };
        return sql.toString();
    }

    /**
     * 该方法暂不实现
     * <p>
     * 构造批量更新数据sql，注意：不会自动填充属性
     *
     * @param map 构造参数
     * @return sql语句
     * @throws RabbiterFrameworkException 框架运行期异常
     * @throws IllegalAccessException     非法权限异常
     */
    @Deprecated
    public String batchUpdate(Map<String, Object> map) throws RabbiterFrameworkException, IllegalAccessException {
        //待更新实体数据集合
        Collection<Object> collection = (Collection<Object>) map.get("collection");
        if (collection.isEmpty()) {
            return "";
        }
        //获取表名
        String tableName = getTableNameFromClazz(map);

        //步骤:使用反射机制加载所有属性
        return "";
    }

    /**
     * 从条件中获取表名
     *
     * @param map 构造sql的条件
     * @return 表名
     * @throws RabbiterFrameworkException 框架运行期异常
     */
    private String getTableNameFromClazz(Map<String, Object> map) throws RabbiterFrameworkException {
        Class clazz = (Class) map.get("clazz");
        Annotation annotation = clazz.getAnnotation(TableName.class);
        String tableName;
        if (ObjectUtils.isEmpty(annotation)) {
            //@TableName注解不存在，将类名转驼峰命名
            tableName = StringUtil.camelHumpToUnderline(String.format("`%s`", clazz.getName()));
        } else {
            //@TableName注解存在
            tableName = String.format("`%s`", ReflectionUtil.getAnnotationValue(annotation, "value"));
        }
        return tableName;
    }

}
