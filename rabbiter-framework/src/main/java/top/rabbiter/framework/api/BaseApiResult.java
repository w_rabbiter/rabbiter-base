package top.rabbiter.framework.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.constant.ApiCode;

import java.io.Serializable;

/**
 * 描述：公共api(报文封装)类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/6
 */
@Data
@ApiModel(description = "接口通用的返回报文")
public class BaseApiResult implements Serializable {
    @ApiModelProperty(value = "状态码", required = true)
    private String code;

    @ApiModelProperty(value = "数据")
    private Object data;

    @ApiModelProperty(value = "错误信息")
    private String errorMsg;

    private BaseApiResult() {
    }

    public BaseApiResult(String code, Object data, String errorMsg) {
        this.code = ObjectUtils.isEmpty(code) ? "" : code;
        this.data = ObjectUtils.isEmpty(data) ? "" : data;
        this.errorMsg = ObjectUtils.isEmpty(errorMsg) ? "" : errorMsg;
    }

    public BaseApiResult(ApiCode apiCode, Object data, String errorMsg) {
        this.code = ObjectUtils.isEmpty(apiCode) ? "" : apiCode.code();
        this.data = ObjectUtils.isEmpty(data) ? "" : data;
        this.errorMsg = ObjectUtils.isEmpty(errorMsg) ? "" : errorMsg;
    }

    /**
     * 成功-携带数据
     *
     * @param data 数据
     * @return 报文
     */
    public static BaseApiResult success(Object data) {
        return setResult(ApiCode.CODE_00000.code(), data, null);
    }

    /**
     * 成功-不携带数据
     *
     * @return 报文
     */
    public static BaseApiResult success() {
        return setResult(ApiCode.CODE_00000.code(), null, null);
    }

    /**
     * 错误-系统执行出错
     *
     * @return 报文
     */
    public static BaseApiResult error() {
        ApiCode apiCode = ApiCode.CODE_B0001;
        return setResult(apiCode.code(), null, apiCode.description());
    }

    /**
     * 错误-用户请求参数错误
     *
     * @return 报文
     */
    public static BaseApiResult paramError() {
        ApiCode apiCode = ApiCode.CODE_A0400;
        return setResult(apiCode.code(), null, apiCode.description());
    }

    /**
     * 错误-系统执行超时
     *
     * @return 报文
     */
    public static BaseApiResult timeOutError() {
        ApiCode apiCode = ApiCode.CODE_B0100;
        return setResult(apiCode.code(), null, apiCode.description());
    }

    /**
     * 接口返回报文,格式:
     * <p>
     * {
     * code:[状态码],
     * data:{
     * [携带数据]
     * },
     * message:[错误信息]
     * }
     *
     * @param code 状态码
     * @param data 携带数据
     * @param msg  错误信息
     * @return 报文结果
     */
    public static BaseApiResult setResult(String code, Object data, String msg) {
        BaseApiResult baseApi = new BaseApiResult();
        baseApi.setCode(code);
        if (!ObjectUtils.isEmpty(data)) {
            baseApi.setData(data);
        } else {
            baseApi.setData("");
        }
        if (!ObjectUtils.isEmpty(msg)) {
            baseApi.setErrorMsg(msg);
        } else {
            baseApi.setErrorMsg("");
        }
        return baseApi;
    }
}
