package top.rabbiter.framework.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.exception.RabbiterFrameworkException;
import top.rabbiter.framework.util.ReflectionUtil;
import top.rabbiter.framework.util.StringUtil;

/**
 * 描述：BaseService统一业务处理器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/11/3
 */
public interface BaseServiceHandler<T> {
    /**
     * 最大批量更新/新增数据行数
     */
    Short MAX_BATCH_ROW = 500;

    /**
     * 封装QueryWrapper的where条件
     *
     * @param t                  属性名
     * @param queryWrapper       查询wrapper
     * @param conditionColumnArr 条件字段名
     */
    default void setWhereCondition(T t, QueryWrapper<T> queryWrapper, String[] conditionColumnArr) {
        for (String conditionColumn : conditionColumnArr) {
            //开始封装where条件
            //转驼峰命名
            String camelhump = StringUtil.underlineToCamelHump(conditionColumn);
            Object fieldValue = null;
            try {
                fieldValue = ReflectionUtil.getFieldValueByName(t, camelhump);
            } catch (RabbiterFrameworkException e) {
                //传入的参数columns中存在错误字段，没有跟数据库的字段名称一一对上
                new RabbiterFrameworkException(String.format("The column : [%s] is not found", conditionColumn))
                        .printStackTrace();
            }
            if (!ObjectUtils.isEmpty(fieldValue)) {
                queryWrapper.eq(conditionColumn, fieldValue);
            }
        }
    }
}
