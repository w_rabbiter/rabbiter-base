package top.rabbiter.framework.annotation;

import top.rabbiter.framework.aop.BaseResponseBodyAdvice;

import java.lang.annotation.*;

/**
 * 标注该注解的控制类或方法
 * <p>
 * 其接口响应值不会被公共接口响应增强器(BaseResponseBodyAdvice)包装
 *
 * @author Rabbiter
 * @version 1.0
 * @see BaseResponseBodyAdvice
 * @since 2020/9/12
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreBaseResponse {

}
