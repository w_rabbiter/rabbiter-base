package top.rabbiter.framework.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 描述：BaseBean相关注解
 * <p>
 * 在源Bean的属性上标注该注解，并指定value
 * <p>
 * value对应目标Bean的属性名，则调用BaseBean中的toBean()方法可以实现自动映射
 *
 * @author Rabbiter
 * @version 1.0
 * @see top.rabbiter.framework.bean.BaseBean
 * @since 2020/10/13
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface BeanField {
    /**
     * value对应目标Bean的属性名，则调用BaseBean中的toBean()方法可以实现自动映射
     * <p>
     * 与ignore方法互斥
     *
     * @return value
     */
    String value() default "";

    /**
     * toBean时是否忽略，默认为false
     * <p>
     * 与value方法互斥
     *
     * @return mapping
     */
    boolean ignore() default false;
}
