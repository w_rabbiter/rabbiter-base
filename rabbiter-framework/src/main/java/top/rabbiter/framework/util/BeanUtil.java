package top.rabbiter.framework.util;

import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * 描述：Bean工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/20
 */
public class BeanUtil {
    /**
     * 拷贝集合里对象的属性
     *
     * @param sourceList 源集合
     * @param targetList 目标集合
     * @param <T>        源集合对象类型
     * @param <F>        目标集合对象类型
     * @param clazz      目标类
     * @throws Exception 实例化对象过程出现异常
     */
    public static <T, F> void copyListProperties(List<T> sourceList, List<F> targetList, Class<F> clazz)
            throws Exception {
        for (T t : sourceList) {
            F f = clazz.newInstance();
            BeanUtils.copyProperties(t, f);
            targetList.add(f);
        }
    }
}
