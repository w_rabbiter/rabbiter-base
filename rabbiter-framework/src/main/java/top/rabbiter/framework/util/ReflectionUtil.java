package top.rabbiter.framework.util;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.Version;
import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.annotation.BeanField;
import top.rabbiter.framework.bean.BaseBean;
import top.rabbiter.framework.exception.RabbiterFrameworkException;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 描述：反射工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class ReflectionUtil {
    /**
     * 根据属性名称获取属性值
     *
     * @param obj       对象
     * @param fieldName 属性名称
     * @return 属性值
     * @throws RabbiterFrameworkException 框架运行期异常
     */
    public static Object getFieldValueByName(Object obj, String fieldName) throws RabbiterFrameworkException {
        Class cls = obj.getClass();
        //得到所有属性
        Field[] fields = cls.getDeclaredFields();
        for (Field item : fields) {
            try {
                //打开私有访问
                item.setAccessible(true);
                if (item.getName().equals(fieldName)) {
                    //返回属性值
                    return item.get(obj);
                }
                //一个个赋值
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        throw new RabbiterFrameworkException(String.format("The field : [%s] is not found", fieldName));
    }

    /**
     * 获取类上的注解的属性值
     *
     * @param annotation  注解
     * @param targetField 属性名
     * @return 属性值
     * @throws RabbiterFrameworkException 框架运行期异常
     */
    public static Object getAnnotationValue(Annotation annotation, String targetField) throws RabbiterFrameworkException {
        Method[] methods = annotation.annotationType().getMethods();
        for (Method method : methods) {
            if (!method.getName().equals(targetField)) {
                continue;
            }
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            try {
                return method.invoke(annotation, null);
            } catch (Exception e) {
                throw new RabbiterFrameworkException(e.getMessage());
            }
        }
        throw new RabbiterFrameworkException(
                String.format("Reflection Error : The field '%s' is not found in @%s",
                        targetField, annotation.toString()));
    }

    /**
     * 生成insert语句的字段列表
     *
     * @param collection 数据集合
     * @return 语句
     */
    public static String batchInsertColumns(Collection<Object> collection) {
        StringBuilder stringBuilder = new StringBuilder();
        int count = 0;
        for (Object object : collection) {
            Class<?> aClass = object.getClass();
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                String column = null;
                TableField annotation = field.getAnnotation(TableField.class);
                if (!ObjectUtils.isEmpty(annotation)) {
                    column = annotation.value();
                }
                if (ObjectUtils.isEmpty(column)) {
                    stringBuilder.append("`").append(field.getName()).append("`,");
                } else {
                    stringBuilder.append("`").append(column).append("`,");
                }
            }
            //去掉最后一个逗号
            stringBuilder.replace(stringBuilder.lastIndexOf(","), stringBuilder.length(), "");
            break;
        }
        return stringBuilder.toString();
    }

    /**
     * (null,'66','2020-09-14 13:15:50','2020-09-14 13:15:50',0),
     * 生成insert语句values后面的语句
     *
     * @param collection 数据集合
     * @param batchSize  插入数量（会忽略collection超出长度的元素，传入null不作限制）
     * @return 语句
     * @throws IllegalAccessException 非法权限异常
     */
    public static String batchInsertValues(Collection<Object> collection, Integer batchSize) throws IllegalAccessException {
        StringBuilder stringBuilder = new StringBuilder();
        int count = 0;
        for (Object object : collection) {
            if (!ObjectUtils.isEmpty(batchSize) && batchSize == count++) {
                //达到最大长度
                new RabbiterFrameworkException(
                        String.format("The batch insert rows is too much , only support below %s , some records has been ignored", batchSize)
                ).printStackTrace();
                break;
            }
            stringBuilder.append("(");
            Class<?> aClass = object.getClass();
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value = field.get(object);
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                //字段值填充
                value = batchInsertFillValue(field, value);
                boolean ifString = value instanceof String || value instanceof LocalDateTime || value instanceof Date;
                if (ifString) {
                    //若是字符串或日期类型，均要加引号
                    stringBuilder.append("'");
                }
                //拼接字段值
                stringBuilder.append(value);
                if (ifString) {
                    //若是字符串或日期类型，均要加引号
                    stringBuilder.append("',");
                } else {
                    stringBuilder.append(",");
                }
            }
            //去掉最后一个逗号
            stringBuilder.replace(stringBuilder.lastIndexOf(","), stringBuilder.length(), "");
            stringBuilder.append("),");
        }
        //去掉第一个左括号
        stringBuilder.replace(stringBuilder.indexOf("("), stringBuilder.indexOf("(") + 1, "");
        //去掉最后一个右括号和逗号
        stringBuilder.replace(stringBuilder.lastIndexOf(")"), stringBuilder.length(), "");
        return stringBuilder.toString();
    }

    /**
     * 字段填充判断
     *
     * @param field 属性（创建时间created、修改时间updated、版本version）
     * @param value 待填充值
     * @return 填充后的值
     */
    private static Object batchInsertFillValue(Field field, Object value) {
        //若用户指定值，则不进行填充
        if (!ObjectUtils.isEmpty(value)) {
            return value;
        }
        //获取填充标记
        TableField tableFieldAnnotation = field.getAnnotation(TableField.class);
        if (ObjectUtils.isEmpty(tableFieldAnnotation)) {
            return value;
        }
        FieldFill fill = tableFieldAnnotation.fill();
        if (ObjectUtils.isEmpty(fill)) {
            return value;
        }

        //填充版本号
        boolean ifFill = fill.equals(FieldFill.UPDATE) || fill.equals(FieldFill.INSERT_UPDATE);
        Version versionAnnotation = field.getAnnotation(Version.class);
        if (!ObjectUtils.isEmpty(versionAnnotation) && ifFill) {
            return 0;
        }

        //填充时间(创建时间/更新时间)
        ifFill = fill.equals(FieldFill.INSERT) || fill.equals(FieldFill.INSERT_UPDATE);
        if (!ifFill) {
            return value;
        }
        String created = "created";
        String updated = "updated";
        if (created.equals(field.getName()) || updated.equals(field.getName())) {
            //填充时间
            return LocalDateTime.now();
        }
        return value;
    }

    /**
     * 根据类对象返回 表对应字段名 的list集合
     * <p>
     * 若clazz中的属性名没有与表字段名对上，注意加上@TableField注解说明
     *
     * @param clazz 类对象
     * @param <F>   对象类型
     * @return list 值：表对应字段名
     */
    public static <F> List<String> getTableFieldsName(Class<F> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        List<String> nameList = new ArrayList<>();
        for (Field field : fields) {
            field.setAccessible(true);
            if ("serialVersionUID".equals(field.getName())) {
                continue;
            }
            TableField tableFieldAnnotation = field.getAnnotation(TableField.class);
            if (!ObjectUtils.isEmpty(tableFieldAnnotation)
                    && !tableFieldAnnotation.exist()) {
                //不是数据库字段
                continue;
            }
            if (!ObjectUtils.isEmpty(tableFieldAnnotation)
                    && !ObjectUtils.isEmpty(tableFieldAnnotation.value())) {
                //若有TableField指定字段，则存入指定的字段
                nameList.add(tableFieldAnnotation.value());
                continue;
            }
            nameList.add(field.getName());
        }
        return nameList;
    }

    /**
     * 获取 属性名-值 的键值对形式
     *
     * @param t   来源实例
     * @param <T> 实体对象类型
     * @return map：属性名-值
     * @throws RabbiterFrameworkException 框架运行期异常
     */
    public static <T extends Serializable> Map<String, Object> getTableFieldValueMap(T t) throws RabbiterFrameworkException {
        Class<? extends Serializable> clazz = t.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Map<String, Object> map = new HashMap<>(fields.length);
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                TableField annotation = field.getAnnotation(TableField.class);
                if (!ObjectUtils.isEmpty(annotation) && !ObjectUtils.isEmpty(annotation.value())) {
                    //@TableField指定表名
                    map.put(annotation.value(), field.get(t));
                } else {
                    //未指定表名，默认属性名
                    map.put(field.getName(), field.get(t));
                }
            } catch (Exception e) {
                e.printStackTrace();
                String errorMsg = String.format("The %s can't be found in class : %s", field.getName(), clazz.getName());
                throw new RabbiterFrameworkException(errorMsg);
            }
        }
        return map;
    }

    /**
     * 将源Bean（source）的属性值映射到目标Bean（target）
     * <p>
     * 针对源Bean（source）中标注了@BeanField的属性
     *
     * @param source 源Bean
     * @param target 目标Bean
     * @param <E>    目标Bean类型
     * @see BeanField
     */
    public static <E> void setToBeanFieldMap(BaseBean source, E target) {
        Class<? extends BaseBean> sourceClazz = source.getClass();
        Field[] sourceFields = sourceClazz.getDeclaredFields();
        for (Field sourceField : sourceFields) {
            sourceField.setAccessible(true);
            BeanField entityField = sourceField.getAnnotation(BeanField.class);
            if (ObjectUtils.isEmpty(entityField)) {
                //无需映射
                continue;
            }
            //获取映射到target的属性名
            String targetFieldName = entityField.value();
            //获取是否进行映射标记
            boolean targetIgnore = entityField.ignore();
            try {
                if (ObjectUtils.isEmpty(targetFieldName)) {
                    //未标记属性名
                    continue;
                }
                Field targetField = target.getClass().getDeclaredField(targetFieldName);
                targetField.setAccessible(true);
                if (!targetIgnore) {
                    //没有忽略标记且指定了属性名
                    targetField.set(target, sourceField.get(source));
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                String errorMsg = String.format("The Field : %s can not be found in target class : %s", targetFieldName, target);
                new RabbiterFrameworkException(errorMsg).printStackTrace();
            }
        }
    }

    /**
     * toBean时获取忽略的属性名称集合
     * <p>
     * 针对源Bean（source）中标注了@BeanField的属性
     *
     * @param source 源Bean
     * @param <E>    源Bean类型
     * @return 忽略属性名称集合
     * @see BeanField
     */
    public static <E> List<String> getToBeanIgnoreField(BaseBean source) {
        List<String> ignoreFieldName = new ArrayList<>();
        Field[] fields = source.getClass().getDeclaredFields();
        for (Field field : fields) {
            BeanField entityField = field.getAnnotation(BeanField.class);
            if (ObjectUtils.isEmpty(entityField)) {
                continue;
            }
            if (entityField.ignore()) {
                //忽略属性
                ignoreFieldName.add(field.getName());
            }
        }
        return ignoreFieldName;
    }
}
