package top.rabbiter.framework.util;

import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.exception.RabbiterFrameworkException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述：Cookie工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/10/12
 */
public class CookieUtil {
    /**
     * 获取cookie
     *
     * @param request 请求
     * @param key     键
     * @return 值
     */
    public static String getCookie(HttpServletRequest request, String key) {
        if (ObjectUtils.isEmpty(request) || ObjectUtils.isEmpty(key)) {
            return null;
        }
        Cookie[] cookies = request.getCookies();
        if(ObjectUtils.isEmpty(cookies)) {
            return null;
        }
        for (Cookie cookie : cookies) {
            if (key.equals(cookie.getName())) {
                return cookie.getValue();
            }
        }
        return null;
    }

    /**
     * 设置Cookie（伪无限期：2年）
     *
     * @param response 响应
     * @param key      键
     * @param value    值
     */
    public static void setCookie(HttpServletResponse response, String key, String value) {
        setCookie(response, key, value);
    }

    /**
     * 设置Cookie
     *
     * @param response  响应
     * @param key       键
     * @param value     值
     * @param maxSecond 有效时间，默认30秒，-1标识关闭浏览器就失效。单位：秒
     */
    public static void setCookie(HttpServletResponse response, String key, String value, Integer maxSecond) {
        if (ObjectUtils.isEmpty(response)) {
            new RabbiterFrameworkException("setCookie() : The response can not be null").printStackTrace();
            return;
        }
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("setCookie() : The cookie's key can not be null").printStackTrace();
            return;
        }
        Cookie cookie = new Cookie(key, value);
        cookie.setPath("/");
        if (!ObjectUtils.isEmpty(maxSecond)) {
            cookie.setMaxAge(maxSecond);
        } else {
            cookie.setMaxAge(60 * 60 * 24 * 365 * 10);
        }
        response.addCookie(cookie);
    }

    /**
     * 删除Cookie
     *
     * @param request  请求
     * @param response 响应
     * @param key      键
     */
    public static void deleteCookie(HttpServletRequest request, HttpServletResponse response, String key) {
        if (ObjectUtils.isEmpty(request)) {
            return;
        }
        if (ObjectUtils.isEmpty(response)) {
            return;
        }
        if (ObjectUtils.isEmpty(key)) {
            return;
        }
        Cookie[] cookies = request.getCookies();
        if(ObjectUtils.isEmpty(cookies)) {
            return;
        }
        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            if (name.equals(key)) {
                cookie.setMaxAge(0);
                response.addCookie(cookie);
                return;
            }
        }
    }

    /**
     * 设置Cookie的有效期
     *
     * @param request   请求
     * @param response  响应
     * @param key       键
     * @param maxSecond 有效期，单位：秒
     */
    public static void setCookieTime(HttpServletRequest request, HttpServletResponse response, String key, Integer maxSecond) {
        if (ObjectUtils.isEmpty(request)) {
            new RabbiterFrameworkException("setCookieTime() : The request can not be null").printStackTrace();
            return;
        }
        if (ObjectUtils.isEmpty(response)) {
            new RabbiterFrameworkException("setCookieTime() : The response can not be null").printStackTrace();
            return;
        }
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("setCookieTime() : The cookie's key can not be null").printStackTrace();
            return;
        }
        Cookie[] cookies = request.getCookies();
        if(ObjectUtils.isEmpty(cookies)) {
            return;
        }
        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            if (name.equals(key)) {
                cookie.setMaxAge(maxSecond);
                response.addCookie(cookie);
                return;
            }
        }
    }

}
