package top.rabbiter.framework.util;

import org.springframework.util.ObjectUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class StringUtil {
    private static final Pattern LINE_PATTERN = Pattern.compile("_[a-z]");
    private static final Pattern HUMP_PATTERN = Pattern.compile("[A-Z]");
    private static final String UNDER_LINE = "_";

    /**
     * 将下划线风格替换为驼峰风格
     *
     * @param str 下划线风格字符串
     * @return 驼峰风格字符串
     */
    public static String underlineToCamelHump(String str) {
        if (!ObjectUtils.isEmpty(str)) {
            Matcher matcher = LINE_PATTERN.matcher(str);
            StringBuilder builder = new StringBuilder(str);
            for (int i = 0; matcher.find(); i++) {
                builder.replace(matcher.start() - i, matcher.end() - i, matcher.group().substring(1).toUpperCase());
            }
            if (Character.isUpperCase(builder.charAt(0))) {
                builder.replace(0, 1, String.valueOf(Character.toLowerCase(builder.charAt(0))));
            }
            return builder.toString();
        }
        return str;
    }

    /**
     * 驼峰转下划线风格
     *
     * @param str 驼峰格式字符串
     * @return 下划线风格字符串
     */
    public static String camelHumpToUnderline(String str) {
        if (!ObjectUtils.isEmpty(str)) {
            Matcher matcher = HUMP_PATTERN.matcher(str);
            StringBuffer builder = new StringBuffer();
            while (matcher.find()) {
                matcher.appendReplacement(builder, "_" + matcher.group(0).toLowerCase());
            }
            matcher.appendTail(builder);
            if(builder.indexOf(UNDER_LINE) == 0) {
                //如果第一个字符是下划线，去掉
                builder.replace(0,1,"");
            }
            return builder.toString();
        }
        return str;
    }

    /**
     * 将字符串首字母大写
     *
     * @param str 字符串
     * @return 首字母大写字符串
     */
    public static String firstToUpper(String str) {
        if (!ObjectUtils.isEmpty(str)) {
            if (str.length() > 1) {
                return Character.toUpperCase(str.charAt(0)) + str.substring(1);
            } else {
                return Character.toUpperCase(str.charAt(0)) + "";
            }
        }
        return "";
    }

    /**
     * 将字符串首字母小写
     *
     * @param str 字符串
     * @return 首字母小写字符串
     */
    public static String firstToLower(String str) {
        return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }

    /**
     * 将包路径转化为文件路径
     *
     * @param packagePath 包路径
     * @return 文件路径
     */
    public static String getPathByPackage(String packagePath) {
        return packagePath.replace(".", "/") + "/";
    }
}
