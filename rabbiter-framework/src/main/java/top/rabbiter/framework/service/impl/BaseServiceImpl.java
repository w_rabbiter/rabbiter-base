package top.rabbiter.framework.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.exception.RabbiterFrameworkException;
import top.rabbiter.framework.handler.BaseServiceHandler;
import top.rabbiter.framework.mapper.BasicMapper;
import top.rabbiter.framework.service.BaseService;
import top.rabbiter.framework.util.ReflectionUtil;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 描述：公共业务类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public abstract class BaseServiceImpl<T extends Serializable, M extends BasicMapper<T>>
        implements BaseService<T>, BaseServiceHandler<T> {
    @Autowired
    private M baseMapper;

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Boolean insert(T t) {
        int row = baseMapper.insert(t);
        return row == 1;
    }

    @Override
    public Object insertReKey(T t) {
        return insertReKey(t, null);
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Object insertReKey(T t, String id) {
        if (ObjectUtils.isEmpty(id)) {
            //设置默认属性
            id = "id";
        }
        if (!insert(t)) {
            return null;
        }
        Object fieldValueByName;
        try {
            //反射获取主键
            fieldValueByName = ReflectionUtil.getFieldValueByName(t, id);
        } catch (RabbiterFrameworkException e) {
            new RabbiterFrameworkException(
                    String.format("Get primary key failed. Can't not found the field : %s in the object : %s", t.toString(), id)
            ).printStackTrace();
            //未正确找到主键，执行回滚
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return null;
        }
        return fieldValueByName;
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Integer batchInsert(Collection<T> collection) {
        if (collection.size() > MAX_BATCH_ROW) {
            new RabbiterFrameworkException(
                    String.format("The batch insert rows is too much , only support below %s", MAX_BATCH_ROW)
            ).printStackTrace();
        }
        return batchInsert(collection, Integer.valueOf(MAX_BATCH_ROW));
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Integer batchInsert(Collection<T> collection, Integer batchSize) {
        if(batchSize <= 0) {
            new RabbiterFrameworkException(
                    "Batch insert failed , the batchSize can't below 1"
            ).printStackTrace();
            return 0;
        }
        return baseMapper.batchInsert(collection,
                (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0],
                batchSize
        );
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Boolean update(T t) {
        int row = baseMapper.updateById(t);
        return row == 1;
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Integer batchUpdate(Collection<T> collection) {
        if (collection.size() > MAX_BATCH_ROW) {
            new RabbiterFrameworkException(
                    String.format("The batch update rows is too much , only support below %s", MAX_BATCH_ROW)
            ).printStackTrace();
        }
        return batchUpdate(collection, Integer.valueOf(MAX_BATCH_ROW));
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Integer batchUpdate(Collection<T> collection, Integer batchSize) {
        if(batchSize <= 0) {
            new RabbiterFrameworkException(
                    "Batch update failed , the batchSize can't below 1"
            ).printStackTrace();
            return 0;
        }
        int row = 0;
        for (T t : collection) {
            row += update(t) ? 1 : 0;
            if (row == batchSize) {
                //达到最大更新数量
                break;
            }
        }
        return row;
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Boolean deleteById(Serializable id) {
        baseMapper.deleteById(id);
        return true;
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Integer batchDeleteByIds(Collection<? extends Serializable> ids) {
        return baseMapper.deleteBatchIds(ids);
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    @Override
    public Integer batchDeleteByColumns(T t, String columns) {
        QueryWrapper<T> wrapper = new QueryWrapper<>();
        String[] conditionColumnArr = columns.split(",");
        //封装where条件
        setWhereCondition(t, wrapper, conditionColumnArr);
        return baseMapper.delete(wrapper);
    }

    @Override
    public T getById(Serializable id) {
        return baseMapper.selectById(id);
    }

    @Override
    public T getById(Serializable id, String resultColumns) {
        return getById(id, null, resultColumns);
    }

    @Override
    public T getById(Serializable id, String primaryKey, String resultColumns) {
        String tableFieldName = ObjectUtils.isEmpty(primaryKey) ? "id" : primaryKey;
        return baseMapper.selectOne(
                new QueryWrapper<T>()
                        .select(resultColumns)
                        .eq(tableFieldName, id)
        );
    }

    @Override
    public List<T> getByColumns() {
        return getByColumns(null, null, null);
    }

    @Override
    public List<T> getByColumns(String resultColumns) {
        return getByColumns(resultColumns, null, null);
    }

    @Override
    public List<T> getByColumns(String resultColumns, T t, String conditionColumns) {
        QueryWrapper<T> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(resultColumns)) {
            //封装查询结果集
            String[] resultColumnArr = resultColumns.split(",");
            wrapper.select(resultColumnArr);
        }
        if (!ObjectUtils.isEmpty(t) && !ObjectUtils.isEmpty(conditionColumns)) {
            //有where条件的话，进行条件封装
            String[] conditionColumnArr = conditionColumns.split(",");
            setWhereCondition(t, wrapper, conditionColumnArr);
        }
        return baseMapper.selectList(wrapper);
    }

    @Override
    public Integer getCount(T t) {
        Map<String, Object> map;
        try {
            map = ReflectionUtil.getTableFieldValueMap(t);
        } catch (RabbiterFrameworkException e) {
            e.printStackTrace();
            return 0;
        }
        QueryWrapper<T> wrapper = new QueryWrapper<>();
        for (String key : map.keySet()) {
            if (!ObjectUtils.isEmpty(map.get(key))) {
                wrapper.eq(key, map.get(key));
            }
        }
        return baseMapper.selectCount(wrapper);
    }

    @Override
    public Integer getCount(QueryWrapper<T> wrapper) {
        if (ObjectUtils.isEmpty(wrapper)) {
            new RabbiterFrameworkException("The Query Condition : wrapper can't be null").printStackTrace();
            return 0;
        }
        return baseMapper.selectCount(wrapper);
    }

    @Override
    public <F> IPage<F> listByPage(IPage<T> iPage, QueryWrapper<T> wrapper, Class<F> clazz) {
        return baseMapper.listByPage(iPage, wrapper, clazz);
    }

    @Override
    public <F> List<F> listByCondition(QueryWrapper<T> wrapper, Class<F> clazz) {
        return baseMapper.listByCondition(wrapper, clazz);
    }
}
