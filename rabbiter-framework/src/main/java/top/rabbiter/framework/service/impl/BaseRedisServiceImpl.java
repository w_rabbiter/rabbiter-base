package top.rabbiter.framework.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import top.rabbiter.framework.exception.RabbiterFrameworkException;
import top.rabbiter.framework.service.BaseRedisService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 描述：公共Redis业务类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/16
 */
public class BaseRedisServiceImpl implements BaseRedisService {

    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 修改序列化方式
     *
     * @param redisTemplate redisTemplate
     */
    @Resource
    public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
        RedisSerializer<String> stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Boolean setString(String key, String data) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setString() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(data)) {
            new RabbiterFrameworkException("BaseRedis setString() error, The data is not null").printStackTrace();
            return false;
        }
        redisTemplate.opsForValue().set(key, data);
        return true;
    }

    @Override
    public Boolean setString(String key, String data, Long timeout) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setString() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(data)) {
            new RabbiterFrameworkException("BaseRedis setString() error, The data is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(timeout)) {
            new RabbiterFrameworkException("BaseRedis setString() error, The timeout is not null").printStackTrace();
            return false;
        }
        redisTemplate.opsForValue().set(key, data, timeout, TimeUnit.SECONDS);
        return true;
    }

    @Override
    public Boolean setIncrement(String key, Long step) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setIncrement() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(step)) {
            new RabbiterFrameworkException("BaseRedis setIncrement() error, The increment's step is not null").printStackTrace();
            return false;
        }
        redisTemplate.opsForValue().increment(key, step);
        return true;
    }

    @Override
    public Boolean setDecrement(String key, Long step) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setDecrement() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(step)) {
            new RabbiterFrameworkException("BaseRedis setDecrement() error, The decrement's step is not null").printStackTrace();
            return false;
        }
        redisTemplate.opsForValue().decrement(key, step);
        return true;
    }

    @Override
    public String getString(String key) {
        Object data = redisTemplate.opsForValue().get(key);
        if (ObjectUtils.isEmpty(data)) {
            return null;
        }
        if (!(data instanceof String)) {
            new RabbiterFrameworkException("BaseRedis getString() error, The result from redis is not be defined a String type").printStackTrace();
            return null;
        }
        return (String) data;
    }

    @Override
    public Map<String, String> listString(String keyPrefix) {
        return listString(keyPrefix, null, null);
    }

    @Override
    public Map<String, String> listString(String keyPrefix, Integer start, Integer end) {
        Map<String, String> map = new HashMap<>();
        if (ObjectUtils.isEmpty(keyPrefix)) {
            new RabbiterFrameworkException("BaseRedis listString() error, The keyPrefix is not null").printStackTrace();
            return map;
        }
        // 获取所有的key
        String prefix = String.format("%s*", keyPrefix);
        Set<String> keys = redisTemplate.keys(prefix);
        if (ObjectUtils.isEmpty(keys) || keys.size() == 0) {
            return map;
        }
        // 获取数据
        for (String key : keys) {
            String value = getString(key);
            map.put(key, value);
        }
        return map;
    }

    @Override
    public Boolean setHash(String key, Map<String, Object> map) {
        return setHash(key, map, null);
    }

    @Override
    public Boolean setHash(String key, Map<String, Object> map, Long timeout) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setHash() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(map)) {
            new RabbiterFrameworkException("BaseRedis setHash() error, The map is not null").printStackTrace();
            return false;
        }
        //将map中的值全部转化为字符串
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            map.put(entry.getKey(), String.valueOf(map.get(entry.getKey())));
        }
        redisTemplate.opsForHash().putAll(key, map);
        if (!ObjectUtils.isEmpty(timeout)) {
            redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
        }
        return true;
    }

    @Override
    public Boolean setHashValue(String key, String hashKey, Object value) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setHashValue() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(hashKey)) {
            new RabbiterFrameworkException("BaseRedis setHashValue() error, The hashKey is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(value)) {
            new RabbiterFrameworkException("BaseRedis setHashValue() error, The value is not null").printStackTrace();
            return false;
        }
        redisTemplate.opsForHash().put(key, hashKey, String.valueOf(value));
        return true;
    }

    @Override
    public Map<String, Object> getHash(String key) {
        Map<Object, Object> entries = redisTemplate.opsForHash().entries(key);
        Map<String, Object> map = new HashMap<>(entries.size());
        if (ObjectUtils.isEmpty(entries)) {
            return map;
        }
        //转json对象
        JSONObject jsonObject = (JSONObject) JSON.toJSON(entries);
        if (!ObjectUtils.isEmpty(jsonObject)) {
            // redis里之前的缓存
            map = JSON.parseObject(JSON.parse(jsonObject.toString()).toString());
        } else {
            new RabbiterFrameworkException("BaseRedis getHash() error, The result from redis is not be defined a hash type").printStackTrace();
        }
        return map;
    }

    @Override
    public Object getHashValue(String key, String hashKey) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setHashValue() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(hashKey)) {
            new RabbiterFrameworkException("BaseRedis setHashValue() error, The hashKey is not null").printStackTrace();
            return false;
        }
        return redisTemplate.opsForHash().get(key, hashKey);
    }

    @Override
    public Map<String, Map<String, Object>> listHash(String keyPrefix) {
        return listHash(keyPrefix, null, null);
    }

    @Override
    public Map<String, Map<String, Object>> listHash(String keyPrefix, Long start, Long end) {
        Map<String, Map<String, Object>> resultMap = new HashMap<>();
        if (ObjectUtils.isEmpty(keyPrefix)) {
            new RabbiterFrameworkException("BaseRedis listMap() error, The keyPrefix is not null").printStackTrace();
            return resultMap;
        }
        // 获取所有的key
        String prefix = String.format("%s*", keyPrefix);
        Set<String> keys = redisTemplate.keys(prefix);
        if (ObjectUtils.isEmpty(keys) || keys.size() == 0) {
            return resultMap;
        }
        // 获取数据
        for (String key : keys) {
            Map<String, Object> map = getHash(key);
            resultMap.put(key, map);
        }
        return resultMap;
    }

    @Override
    public Boolean setIncrementInHash(String key, String hashKey, Long step) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setIncrementInHash() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(hashKey)) {
            new RabbiterFrameworkException("BaseRedis setIncrementInHash() error, The increment's hashKey is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(step)) {
            new RabbiterFrameworkException("BaseRedis setIncrementInHash() error, The increment's step is not null").printStackTrace();
            return false;
        }
        redisTemplate.opsForHash().increment(key, hashKey, step);
        return true;
    }

    @Override
    public Boolean setDecrementInHash(String key, String hashKey, Long step) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis setDecrementInHash() error, The key is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(hashKey)) {
            new RabbiterFrameworkException("BaseRedis setDecrementInHash() error, The decrement's hashKey is not null").printStackTrace();
            return false;
        }
        if (ObjectUtils.isEmpty(step)) {
            new RabbiterFrameworkException("BaseRedis setDecrementInHash() error, The decrement's step is not null").printStackTrace();
            return false;
        }
        redisTemplate.opsForHash().increment(key, hashKey, step * -1);
        return true;
    }

    @Override
    public Boolean delete(String key) {
        if (ObjectUtils.isEmpty(key)) {
            new RabbiterFrameworkException("BaseRedis delete() error, The key is not null").printStackTrace();
            return false;
        }
        return redisTemplate.delete(key);
    }

    @Override
    public Boolean batchDelete(String keyPrefix) {
        Set<String> keys = redisTemplate.keys(String.format("%s*", keyPrefix));
        if (ObjectUtils.isEmpty(keys)) {
            return true;
        }
        redisTemplate.delete(keys);
        return true;
    }
}
