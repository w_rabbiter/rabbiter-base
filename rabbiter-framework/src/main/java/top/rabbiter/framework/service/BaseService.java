package top.rabbiter.framework.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 公共业务接口
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/8
 */
public interface BaseService<T> {
    /**
     * 插入数据
     *
     * @param t 数据对象
     * @return 是否插入成功
     */
    Boolean insert(T t);

    /**
     * 插入数据并返回新主键，主键在对象T中的属性对应必须为id
     *
     * @param t 数据对象
     * @return 主键，若插入失败时返回null
     */
    Object insertReKey(T t);

    /**
     * 插入数据并返回新主键
     *
     * @param t  数据对象
     * @param id 主键在对象中的属性名，默认取“id”
     * @return 主键，若插入失败时返回null
     */
    Object insertReKey(T t, String id);

    /**
     * 批量插入数据（限定500条数据） - 目前仅适用与mysql
     *
     * @param collection 插入数据集合。限定长度500
     * @return 插入行数
     */
    Integer batchInsert(Collection<T> collection);

    /**
     * 批量插入数据 - 目前仅适用与mysql
     *
     * @param collection 插入数据集合
     * @param batchSize  插入数量（忽略collection超出的元素）
     * @return 插入行数
     */
    Integer batchInsert(Collection<T> collection, Integer batchSize);

    /**
     * 根据主键更新数据
     *
     * @param t 更新数据对象
     * @return 是否更新成功
     */
    Boolean update(T t);

    /**
     * 批量更新数据
     *
     * @param collection 更新数据集合。限定长度500
     * @return 更新行数
     */
    Integer batchUpdate(Collection<T> collection);


    /**
     * 批量更新数据
     *
     * @param collection 更新数据集合
     * @param batchSize  更新数量（忽略collection超出的元素）
     * @return 更新行数
     */
    Integer batchUpdate(Collection<T> collection, Integer batchSize);

    /**
     * 根据主键删除数据
     *
     * @param id 主键
     * @return 是否删除成功
     */
    Boolean deleteById(Serializable id);

    /**
     * 根据主键批量删除数据
     *
     * @param ids 主键列表
     * @return 删除行数
     */
    Integer batchDeleteByIds(Collection<? extends Serializable> ids);

    /**
     * 根据列值批量删除数据
     *
     * @param t       实体，保存字段值，跟columns要对上
     * @param columns 指定删除的字段名，传入格式："id,name,sex"
     * @return 删除行数
     */
    Integer batchDeleteByColumns(T t, String columns);

    /**
     * 根据主键获取对象
     *
     * @param id 主键
     * @return 对象
     */
    T getById(Serializable id);

    /**
     * 根据主键获取对象（自定义返回结果，且表中对应主键字段为id）
     *
     * @param id            主键
     * @param resultColumns 返回结果，传入格式："id,name,sex"
     * @return 对象
     */
    T getById(Serializable id, String resultColumns);

    /**
     * 根据主键获取对象（自定义返回结果和在表中的主键名称）
     *
     * @param id            主键
     * @param primaryKey    主键名称
     * @param resultColumns 返回结果，传入格式："id,name,sex"
     * @return 对象
     */
    T getById(Serializable id, String primaryKey, String resultColumns);

    /**
     * 返回所有列表
     *
     * @return 列表（所有列）
     */
    List<T> getByColumns();

    /**
     * 返回指定列的列表
     *
     * @param resultColumns 返回的字段，传入格式："id,name,sex"
     * @return 列表（指定列）
     */
    List<T> getByColumns(String resultColumns);

    /**
     * 根据指定列和条件查询列表
     *
     * @param resultColumns    返回的字段，传入格式："id,name,sex"
     * @param t                封装查询条件的数据，跟传入的columns字段要对上
     * @param conditionColumns 条件的字段，传入格式："id,name,sex"
     * @return 列表（指定列+相等条件）
     */
    List<T> getByColumns(String resultColumns, T t, String conditionColumns);

    /**
     * 获取记录数量（全部都是相等条件）
     *
     * @param t 相等条件
     * @return 记录数量
     */
    Integer getCount(T t);

    /**
     * 获取记录数量（自定义条件）
     *
     * @param wrapper 自定义查询条件
     * @return 记录数量
     */
    Integer getCount(QueryWrapper<T> wrapper);

    /**
     * 分页增强：可以指定查询返回的对象类型（要与实体类属性有所对应）
     * <p>
     * 若目标对象clazz中的属性名没有与表字段名对上，注意加上@TableField注解说明
     *
     * @param iPage   分页条件
     * @param wrapper 查询条件
     * @param clazz   对象F类对象
     * @param <F>     对象类型为F的列表
     * @return 分页结果集
     */
    <F> IPage<F> listByPage(IPage<T> iPage, QueryWrapper<T> wrapper, Class<F> clazz);

    /**
     * 列表增强：可以指定查询返回的对象类型（要与实体类属性有所对应）
     *
     * @param wrapper 查询条件
     * @param clazz   对象F类对象
     * @param <F>     对象类型为F的列表
     * @return 列表
     */
    <F> List<F> listByCondition(QueryWrapper<T> wrapper, Class<F> clazz);
}
