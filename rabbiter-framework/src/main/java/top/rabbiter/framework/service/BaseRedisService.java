package top.rabbiter.framework.service;

import java.util.Map;

/**
 * 描述：公共Redis业务接口
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/16
 */
public interface BaseRedisService {
    /**
     * 存入字符串类型（无期限）
     *
     * @param key  键
     * @param data 字符串
     * @return 是否存入成功
     */
    Boolean setString(String key, String data);

    /**
     * 存入字符串类型（设置过期时间）
     *
     * @param key     键
     * @param data    字符串
     * @param timeout 设置过期时间，单位秒
     * @return 是否存入成功
     */
    Boolean setString(String key, String data, Long timeout);

    /**
     * 自增1
     *
     * @param key  键
     * @param step 步长
     * @return 是否自增成功
     */
    Boolean setIncrement(String key, Long step);

    /**
     * 自减1
     *
     * @param key  键
     * @param step 步长
     * @return 是否自减成功
     */
    Boolean setDecrement(String key, Long step);

    /**
     * 获取字符串数据
     *
     * @param key 键
     * @return 字符串数据
     */
    String getString(String key);

    /**
     * 获取截取指定下标字符串list
     *
     * @param keyPrefix key前缀
     * @param start     起始坐标
     * @param end       结束坐标
     * @return map：键-字符串
     */
    Map<String, String> listString(String keyPrefix, Integer start, Integer end);

    /**
     * 获取全部字符串list
     *
     * @param keyPrefix key前缀
     * @return map：键-字符串
     */
    Map<String, String> listString(String keyPrefix);

    /**
     * 存放哈希结构（无期限）
     *
     * @param key 键
     * @param map map集合
     * @return 是否存放成功
     */
    Boolean setHash(String key, Map<String, Object> map);

    /**
     * 存放哈希结构（有效期）
     *
     * @param key     键
     * @param map     map集合
     * @param timeout 过期时间（单位：秒）
     * @return 是否存放成功
     */
    Boolean setHash(String key, Map<String, Object> map, Long timeout);

    /**
     * 修改hash里的某个键对应的值
     *
     * @param key     键
     * @param hashKey 哈希键
     * @param value   修改后的值
     * @return 是否修改成功
     */
    Boolean setHashValue(String key, String hashKey, Object value);

    /**
     * 获取哈希结构
     *
     * @param key 键
     * @return map
     */
    Map<String, Object> getHash(String key);

    /**
     * 获取哈希里的某个键对应的值
     *
     * @param key     键
     * @param hashKey 哈希键
     * @return 值
     */
    Object getHashValue(String key, String hashKey);

    /**
     * 批量获取哈希结构（获取所有）
     *
     * @param keyPrefix 键前缀
     * @return hash结构的键值对
     */
    Map<String, Map<String, Object>> listHash(String keyPrefix);

    /**
     * 批量获取哈希结构（带截取坐标）
     *
     * @param keyPrefix 键前缀
     * @param start     起始下标
     * @param end       终止下标
     * @return hash结构的键值对
     */
    Map<String, Map<String, Object>> listHash(String keyPrefix, Long start, Long end);

    /**
     * 自增hash结构里的值
     *
     * @param key     键
     * @param hashKey 哈希键
     * @param step    自增步长
     * @return 是否自增成功
     */
    Boolean setIncrementInHash(String key, String hashKey, Long step);

    /**
     * 自减hash结构里的值
     *
     * @param key     键
     * @param hashKey 哈希键
     * @param step    自减步长
     * @return 是否自减成功
     */
    Boolean setDecrementInHash(String key, String hashKey, Long step);

    /**
     * 移除数据
     *
     * @param key 键
     * @return 是否移除成功
     */
    Boolean delete(String key);

    /**
     * 批量移除
     *
     * @param keyPrefix 键前缀
     * @return 是否移除成功
     */
    Boolean batchDelete(String keyPrefix);
}
