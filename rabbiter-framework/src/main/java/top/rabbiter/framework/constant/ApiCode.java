package top.rabbiter.framework.constant;

/**
 * 描述：接口状态码常量
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public enum ApiCode {
    //api报文状态码常量
    CODE_00000("00000", "一切OK"),
    CODE_A0001("A0001", "用户端错误"),
    CODE_A0100("A0100", "用户注册错误"),
    CODE_A0132("A0132", "邮件校验码错误"),
    CODE_A0301("A0301", "用户未授权"),
    CODE_A0440("A0440", "用户操作异常"),
    CODE_A0700("A0700", "用户上传文件异常"),
    CODE_A0701("A0701", "用户上传文件类型不匹配"),
    CODE_A0702("A0700", "用户上传文件太大"),
    CODE_A0703("A0700", "用户上传图片太大"),
    CODE_A0704("A0700", "用户上传视频太大"),
    CODE_A0705("A0700", "用户上传压缩文件太大"),
    CODE_A0400("A0400", "用户请求参数错误"),
    CODE_A0201("A0201", "用户账户不存在"),
    CODE_A0202("A0202", "用户账户被冻结"),
    CODE_A0210("A0210", "用户密码错误"),
    CODE_A0230("A0230", "登录已过期"),
    CODE_B0001("B0001", "系统执行出错"),
    CODE_B0100("B0100", "系统执行超时"),
    CODE_C0250("C0250", "数据库服务超时"),
    CODE_C0300("C0300", "数据库服务出错"),
    CODE_C0001("C0001", "调用第三方服务出错"),
    CODE_C0130("C0130", "缓存服务出错"),
    CODE_C0503("C0503", "邮件提醒服务失败");

    ApiCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    private String code;
    private String description;

    public String code() {
        return code;
    }

    public String description() {
        return description;
    }
}
