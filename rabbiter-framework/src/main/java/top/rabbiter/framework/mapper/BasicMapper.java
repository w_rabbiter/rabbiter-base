package top.rabbiter.framework.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;
import top.rabbiter.framework.exception.RabbiterFrameworkException;
import top.rabbiter.framework.provider.BaseProvider;
import top.rabbiter.framework.util.BeanUtil;
import top.rabbiter.framework.util.ReflectionUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 描述：基础mapper
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/14
 */
@Repository
public interface BasicMapper<T> extends BaseMapper<T> {
    /**
     * 批量插入数据
     * <p>
     * 注意：本方法目前仅支持mysql
     * <p>
     * 且实体类中属性与数据库字段没有对上，需要在实体类中标注@TableField(value = "")指明
     *
     * @param collection 数据集合
     * @param clazz      对应实体的类对象
     * @param batchSize  插入行数
     * @return 修改行数
     */
    @InsertProvider(type = BaseProvider.class, method = "batchInsert")
    Integer batchInsert(
            @Param("collection") Collection<T> collection, @Param("clazz") Class clazz, @Param("batchSize") Integer batchSize
    );

    /**
     * 批量更新数据
     * <p>
     * 注意：本方法目前仅支持mysql
     * <p>
     * 且实体类中属性与数据库字段没有对上，需要在实体类中标注@TableField(value = "")指明
     *
     * @param collection 数据集合
     * @param clazz      对应实体的类对象
     * @return 修改行数
     */
    @Deprecated
    @UpdateProvider(type = BaseProvider.class, method = "batchUpdate")
    Integer batchUpdate(@Param("collection") Collection<T> collection, @Param("clazz") Class clazz);

    /**
     * 分页增强：可以指定查询返回的对象类型（要与实体类属性有所对应）
     * <p>
     * 若目标对象clazz中的属性名没有与表字段名对上，注意加上@TableField注解说明
     *
     * @param iPage   分页条件
     * @param wrapper 查询条件
     * @param clazz   对象F类对象
     * @param <F>     对象类型为F的列表
     * @return 分页结果集
     */
    default <F> IPage<F> listByPage(IPage<T> iPage, QueryWrapper<T> wrapper, Class<F> clazz) {
        List<String> fieldNameList = ReflectionUtil.getTableFieldsName(clazz);
        //封装查询的字段
        StringBuilder stringBuilder = new StringBuilder();
        for (String fieldName : fieldNameList) {
            stringBuilder.append(fieldName).append(",");
        }
        //去除最后一个逗号
        stringBuilder.replace(stringBuilder.lastIndexOf(","), stringBuilder.length(), "");
        wrapper.select(stringBuilder.toString());
        //执行分页查询
        IPage<T> page = selectPage(iPage, wrapper);
        List<T> records = page.getRecords();
        List<F> targetRecords = new ArrayList<>();
        try {
            BeanUtil.copyListProperties(records, targetRecords, clazz);
        } catch (Exception e) {
            e.printStackTrace();
            String errorMsg = String.format("Type can't be transfer to %s", clazz.getTypeName());
            new RabbiterFrameworkException(errorMsg).printStackTrace();
        }
        //封装新Page对象
        IPage<F> resultPage = new Page<>();
        resultPage.setSize(page.getSize());
        resultPage.setCurrent(page.getCurrent());
        resultPage.setPages(page.getPages());
        resultPage.setTotal(page.getTotal());
        resultPage.setRecords(targetRecords);
        return resultPage;
    }

    /**
     * 列表增强
     * <p>
     * 若目标对象clazz中的属性名没有与表字段名对上，注意加上@TableField注解说明
     *
     * @param wrapper 查询条件
     * @param clazz   对象F类对象
     * @param <F>     对象类型为F的列表
     * @return 列表
     */
    default <F> List<F> listByCondition(QueryWrapper<T> wrapper, Class<F> clazz) {
        List<String> fieldNameList = ReflectionUtil.getTableFieldsName(clazz);
        //封装查询的字段
        StringBuilder stringBuilder = new StringBuilder();
        for (String fieldName : fieldNameList) {
            stringBuilder.append(fieldName).append(",");
        }
        //去除最后一个逗号
        stringBuilder.replace(stringBuilder.lastIndexOf(","), stringBuilder.length(), "");
        wrapper.select(stringBuilder.toString());
        //执行分页查询
        List<T> resultList = selectList(wrapper);
        //目标列表
        List<F> targetRecords = new ArrayList<>();
        try {
            BeanUtil.copyListProperties(resultList, targetRecords, clazz);
        } catch (Exception e) {
            e.printStackTrace();
            String errorMsg = String.format("Type can't be transfer to %s", clazz.getTypeName());
            new RabbiterFrameworkException(errorMsg).printStackTrace();
        }
        return targetRecords;
    }
}
