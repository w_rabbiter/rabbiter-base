package top.rabbiter.framework.mapper;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import top.rabbiter.framework.bean.ElasticSearchBean;

/**
 * 描述：ElasticSearch公共mapper
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2021/1/10
 */
public interface ElasticSearchBasicMapper<T extends ElasticSearchBean> extends ElasticsearchRepository<T, Long> {

}
