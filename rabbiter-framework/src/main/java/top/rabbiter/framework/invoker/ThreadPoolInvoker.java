package top.rabbiter.framework.invoker;

import com.google.common.collect.Maps;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.constant.ApiCode;
import top.rabbiter.framework.exception.ApiException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 描述：线程池管理执行器
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2021/1/7
 */
public class ThreadPoolInvoker {
    private static final Map<String, ThreadPoolExecutor> POOLS = Maps.newHashMap();

    /**
     * 执行异步任务
     *
     * @param threadNamePrefix 线程池中线程前缀（线程池唯一标识）
     * @param task             线程任务
     * @param cls              Class对象
     * @param <R>              泛型
     * @return 异步任务执行结果
     */
    public <R extends Class<Object>> Future<R> submit(String threadNamePrefix, Runnable task, R cls) {
        if (!POOLS.containsKey(threadNamePrefix)) {
            throw new ApiException(ApiCode.CODE_B0001, String.format("The thread pool that thread-name-prefix %s is not defined", threadNamePrefix));
        }

        return POOLS.get(threadNamePrefix).submit(task, cls);
    }

    /**
     * 执行同步任务
     *
     * @param threadNamePrefix 线程池中线程前缀（线程池唯一标识）
     * @param task             线程任务
     */
    public void run(String threadNamePrefix, Runnable task) {
        if (!POOLS.containsKey(threadNamePrefix)) {
            throw new ApiException(ApiCode.CODE_B0001, String.format("The thread pool that thread-name-prefix [%s] is not defined", threadNamePrefix));
        }
        POOLS.get(threadNamePrefix).execute(task);
    }

    /**
     * 构造线程池
     *
     * @param threadNamePrefix 线程池线程前缀。必传
     * @param capacity         阻塞队列容量。默认10
     * @param keepAliveTime    线程空闲存活时长。单位：分钟。默认10
     * @return 本实例
     */
    public ThreadPoolInvoker build(String threadNamePrefix, Integer capacity, Integer keepAliveTime) {
        if (ObjectUtils.isEmpty(capacity)) {
            capacity = 10;
        }
        if (ObjectUtils.isEmpty(keepAliveTime)) {
            keepAliveTime = 10;
        }
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                Runtime.getRuntime().availableProcessors(),
                Runtime.getRuntime().availableProcessors() * 2,
                keepAliveTime,
                TimeUnit.MINUTES,
                new LinkedBlockingQueue<>(capacity),
                new CustomizableThreadFactory(threadNamePrefix),
                new ThreadPoolExecutor.AbortPolicy()
        );
        POOLS.put(threadNamePrefix, threadPoolExecutor);
        return this;
    }

    /**
     * 构造线程池
     *
     * @param threadNamePrefix 线程池线程前缀。必传
     * @return 本实例
     */
    public ThreadPoolInvoker build(String threadNamePrefix) {
        return this.build(threadNamePrefix, null, null);
    }
}
