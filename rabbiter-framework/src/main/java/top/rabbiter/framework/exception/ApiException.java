package top.rabbiter.framework.exception;

import top.rabbiter.framework.constant.ApiCode;

/**
 * 公共接口异常类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/11
 */
public class ApiException extends RuntimeException {
    /**
     * 错误码
     */
    private final String code;

    /**
     * 异常信息
     */
    private final String message;

    /**
     * 传入自定义状态码异常
     *
     * @param code    自定义状态码
     * @param message 错误信息
     */
    public ApiException(String code, String message) {
        super(message);
        this.message = message;
        this.code = code;
    }

    /**
     * 传入rabbiter-framework提供的状态码异常
     *
     * @param apiCode rabbiter-framework提供的状态码常量
     * @param message     错误信息
     */

    public ApiException(ApiCode apiCode, String message) {
        super(message);
        this.message = message;
        this.code = apiCode.code();
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}
