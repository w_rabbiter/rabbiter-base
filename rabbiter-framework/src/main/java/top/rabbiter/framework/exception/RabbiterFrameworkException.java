package top.rabbiter.framework.exception;

/**
 * rabbiter-framework开发框架运行期异常
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/9
 */
public class RabbiterFrameworkException extends Exception {
    public RabbiterFrameworkException(String message) {
        super(String.format("RABBITER FRAMEWORK ERROR >>> %s", message));
    }
}
