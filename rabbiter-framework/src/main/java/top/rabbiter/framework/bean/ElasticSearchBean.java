package top.rabbiter.framework.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

/**
 * 描述：ElasticSearch公共实体
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2021/1/10
 */
@Getter
@Setter
@ToString
public class ElasticSearchBean implements BaseBean {
    @Id
    private Long id;
}
