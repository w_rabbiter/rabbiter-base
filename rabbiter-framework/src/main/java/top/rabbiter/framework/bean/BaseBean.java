package top.rabbiter.framework.bean;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;
import top.rabbiter.framework.annotation.BeanField;
import top.rabbiter.framework.exception.RabbiterFrameworkException;
import top.rabbiter.framework.util.ReflectionUtil;

import java.io.*;
import java.util.List;

/**
 * 描述：公共Bean接口
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/24
 */
public interface BaseBean extends Serializable {
    /**
     * 转化Bean
     * <p>
     * 浅拷贝
     * <p>
     * 相关注解：@BeanField
     *
     * @param clazz 目标Bean的class对象
     * @param <E>   目标Bean
     * @return 目标Bean
     * @see BeanField
     */
    default <E> E toBean(Class<E> clazz) {
        if (ObjectUtils.isEmpty(clazz)) {
            new RabbiterFrameworkException("Transfer Bean failed. The parameter : clazz is null").printStackTrace();
        }
        E e = null;
        try {
            e = clazz.newInstance();
            //获取忽略的属性
            List<String> ignoreFields = ReflectionUtil.getToBeanIgnoreField(this);
            BeanUtils.copyProperties(this, e, ignoreFields.toArray(new String[0]));
            //将源Bean（this）的属性映射到目标Bean（e）。针对源Bean中标注了@EntityField的属性
            ReflectionUtil.setToBeanFieldMap(this, e);
        } catch (Exception exception) {
            exception.printStackTrace();
            new RabbiterFrameworkException("Transfer Bean failed.").printStackTrace();
        }
        return e;
    }

    /**
     * 深拷贝对象
     * <p>
     * 序列化方式深克隆对象
     *
     * @param clazz 类对象。必须与当前实例相同
     * @param <E>   泛型
     * @return 深拷贝后的对象
     */
    default <E> E deepClone(Class<E> clazz) {
        //检查class参数是否合法
        E e;
        try {
            e = clazz.newInstance();
            if (!e.getClass().equals(this.getClass())) {
                new RabbiterFrameworkException(
                        String.format("Deep copy Bean failed. The parameter : class must be type [%s]", e.getClass().getName())
                ).printStackTrace();
                return null;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            new RabbiterFrameworkException("Deep copy Bean failed.").printStackTrace();
        }

        //创建流对象
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            //序列化
            oos.writeObject(this);
            //反序列化
            bis = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bis);
            //序列化
            e = (E) ois.readObject();
            return e;
        } catch (Exception exception) {
            exception.printStackTrace();
            new RabbiterFrameworkException("Deep copy Bean failed.").printStackTrace();
            return null;
        } finally {
            //关闭流
            try {
                if (bis != null) {
                    bis.close();
                }
                if (ois != null) {
                    ois.close();
                }
            } catch (Exception exception) {
                exception.printStackTrace();
                new RabbiterFrameworkException("Deep copy Bean failed.").printStackTrace();
            }
        }
    }

    /**
     * 深拷贝对象
     * <p>
     * 序列化方式深克隆对象
     *
     * @return 深拷贝后的对象
     */
    default Object deepClone() {
        return deepClone(this.getClass());
    }

    /**
     * 转成json字符串
     *
     * @return json字符串
     */
    default String toJson() {
        return JSON.toJSONString(this);
    }
}
