package top.rabbiter.framework;

import top.rabbiter.framework.invoker.ThreadPoolInvoker;
import top.rabbiter.framework.test.Test1;
import top.rabbiter.framework.test.Test2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 描述：Rabbiter框架测试类
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/9/3
 */
public class TestFrameWork {
    public static void main(String[] args) {
//        Test2 test2 = new Test2().setAge(1);
//        Test1 copy = (Test1) new Test1().setTest2(test2).deepClone();
//        test2.setAge(2);
//        System.out.println(copy.toJson());


        ThreadPoolInvoker threadPoolInvoker = new ThreadPoolInvoker();
        threadPoolInvoker.build("thread-").run("thread-", () -> {
            System.out.println(123);
        });


        Executors.newCachedThreadPool();
        Executors.newFixedThreadPool(10);
        Executors.newSingleThreadExecutor();
    }
}




