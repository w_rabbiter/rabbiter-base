package top.rabbiter.framework.test;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import top.rabbiter.framework.bean.BaseBean;

/**
 * @author WuZhengHua
 * @version 1.0
 * @since 2020/11/23
 */
@Getter
@Setter
@Accessors(chain =  true)
@ToString
public class Test1 implements BaseBean {

    private Test2 test2;
}
