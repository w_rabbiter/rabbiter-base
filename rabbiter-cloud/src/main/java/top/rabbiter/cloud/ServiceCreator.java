package top.rabbiter.cloud;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.util.ObjectUtils;
import top.rabbiter.cloud.annotation.CloudService;

/**
 * 描述：Feign构造者
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/10/16
 */
@Import(FeignClientsConfiguration.class)
public class ServiceCreator {
//    private UserFeign userFeign;

    @Autowired
    private ApplicationContext applicationContext;

    public void install() {

    }

    @Autowired
    public ServiceCreator(Decoder decoder, Encoder encoder, Client client, Contract contract) {

        String[] beanNames = applicationContext.getBeanDefinitionNames();
        for (String beanName : beanNames) {
            Object bean = applicationContext.getBean(beanName);
            Class<?> aClass = bean.getClass();
            CloudService cloudService = aClass.getAnnotation(CloudService.class);
            if (!ObjectUtils.isEmpty(cloudService)) {
                //需要生成Feign
                Object target = Feign.builder().client(client).encoder(encoder).decoder(decoder).contract(contract)
//                .requestInterceptor(new BasicAuthRequestInterceptor("user","123456"))
                        .target(aClass, "http://rb-user/");
                BeanFactory parentBeanFactory = applicationContext.getParentBeanFactory();
            }
        }
    }

//    @Bean
//    public UserFeign userFeign() {
//        return this.userFeign;
//    }

//    private List<MetadataReader> scanPackage() throws Exception{
//        ResourcePatternResolver resolver = ResourcePatternUtils.getResourcePatternResolver(resourceLoader);
//        MetadataReaderFactory metaReader = new CachingMetadataReaderFactory(resourceLoader);
//        Resource[] resources = resolver.getResources(debugProperties.getPackagePath());
//        List<MetadataReader> readers = new ArrayList<>();
//        for (Resource r : resources) {
//            MetadataReader reader = metaReader.getMetadataReader(r);
//            readers.add(reader);
//        }
//        return readers;
//    }
}
