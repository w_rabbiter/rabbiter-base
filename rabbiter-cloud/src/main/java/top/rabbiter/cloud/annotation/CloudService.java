package top.rabbiter.cloud.annotation;

import java.lang.annotation.*;

/**
 * 描述：标注为业务提供者的注解
 *
 * @author Rabbiter
 * @version 1.0
 * @since 2020/10/16
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface CloudService {
    String value();
}
